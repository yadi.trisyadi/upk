@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Pengaturan @endslot
@slot('title') Tambah Desa/Kelurahan @endslot
@endcomponent
<form action="{{ route('pengaturan-desa.store') }}"  method="post" class="custom-validation">
    @csrf
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">

                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="nama">Nama Desa/Kelurahan</label>
                            <select class="form-control select2" name="desa" id="desa" required>
                                <option value="" selected disabled>-- Pilih Kelurahan/Desa</option>
                                @foreach ($desaCode as $desaCode)
                                    <option value="{{ $desaCode->code }}">{{ $desaCode->name }}</option>
                                @endforeach
                                <option value="other">Lainnya</option>
                            </select>
                        </div>
                        <div class="mb-3" id="fieldLainnya" style="display: none;">
                            <label for="desaLainnya">Kelurahan/Desa Lainnya</label>
                            <input type="text" class="form-control" name="desaLainnya" id="desaLainnya" placeholder="Lainnya">
                        </div>
                        <div class="mb-3">
                            <label for="alamat">Alamat Kantor</label>
                            <input id="alamat" name="alamat" type="text" class="form-control" required>
                        </div>
                        <div class="mb-3">
                            <label for="telp">Telp</label>
                            <input id="telp" name="telp" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="kepalaDesa">Kepala Desa / Lurah</label>
                            <input id="kepalaDesa" name="kepalaDesa" type="text" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="status">Status</label>
                            <select name="status" class="form-control select2" required>
                                <option value="kelurahan">Kelurahan</option>
                                <option value="desa">Desa</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                        <button type="button" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
    <!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script type="text/javascript">
    $('#desa').change(function(){
        var desa = $('#desa').val();
        if(desa == 'other'){
           $('#fieldLainnya').show();
           $('#desaLainnya').attr('required',true);
        }else{
            $('#fieldLainnya').hide();
            $('#desaLainnya').val('');
             $('#desaLainnya').attr('required',false);
        }
    })
  </script>

@endsection

