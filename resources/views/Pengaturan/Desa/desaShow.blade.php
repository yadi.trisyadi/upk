@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Pengaturan @endslot
        @slot('title') Desa @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                        <div>{{ $message }}</div>
                    </div>
	                @endif
                    {{-- <h4 class="card-title">Default Datatable</h4>
                    <p class="card-title-desc">DataTables has most features enabled by
                        default, so all you need to do to use it with your own tables is to call
                        the construction function: <code>$().DataTable();</code>.
                    </p> --}}
                    <p>


                        <a class="btn btn-primary btn-sm" href="{{ route('pengaturan-desa.create') }}">Tambah</a>
                    </p>
                    <table id="datatable" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                            <tr>
                                <th>Kode</th>
                                <th>Nama Desa</th>
                                <th>Alamat</th>
                                <th>Telp</th>
                                <th>Kepala Desa/Lurah</th>
                                <th>Jenis</th>
                                <th>Action</th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach ($data as $data )
                            <tr>
                                <td>{{ $data->kode }}</td>
                                <td>{{ $data->nama_desa }}</td>
                                <td>{{ $data->alamat_desa }}</td>
                                <td>{{ $data->telp }}</td>
                                <td>{{ $data->kepala_desa }}</td>
                                <td>{{ ucfirst($data->status) }}</td>
                                <td>
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('pengaturan-desa.edit',$data->id) }}">Edit</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
