@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Pengaturan @endslot
        @slot('title') Profil @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                        <div>{{ $message }}</div>
                    </div>
	                @endif
                    {{-- <h4 class="card-title">Default Datatable</h4>
                    <p class="card-title-desc">DataTables has most features enabled by
                        default, so all you need to do to use it with your own tables is to call
                        the construction function: <code>$().DataTable();</code>.
                    </p> --}}
                    <p>
                        @if ($count < 1)

                        <a class="btn btn-primary btn-sm" href="{{ route('pengaturan-profile.create') }}">Tambah</a>
                        @endif
                    </p>
                    <table id="datatable" class="table table-bordered dt-responsive  nowrap w-100">
                        <thead>
                            <tr>
                                <th>Nama Upk</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Email</th>
                                <th>Website</th>
                                <th>Action</th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach ($data as $data )
                            <tr>
                                <td>{{ $data->nama_upk }}</td>
                                <td>{{ $data->alamat_upk }} {{ $data->kota }} {{ $data->propinsi }}</td>
                                <td>{{ $data->telp_upk }}</td>
                                <td>{{ $data->email_upk }}</td>
                                <td>{{ $data->website_upk }}</td>
                                <td>
                                    <a class="btn btn-outline-primary btn-sm" href="{{ route('pengaturan-profile.edit',$data->id) }}">Edit</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
