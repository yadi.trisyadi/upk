@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Pengaturan @endslot
@slot('title') Tambah Profile @endslot
@endcomponent
<form action="{{ route('pengaturan-profile.store') }}"  enctype="multipart/form-data" method="post" class="custom-validation">
    @csrf
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Profil</h4>
                <p class="card-title-desc">Silahkan Isi Form Dibawah Ini</p>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="productname">Kode</label>
                            <input id="productname" name="kodeUpk" type="text" class="form-control" required>
                        </div>
                        <div class="mb-3">
                            <label for="manufacturername">Nama UPK</label>
                            <input id="manufacturername" name="namaUpk" type="text" class="form-control" required>
                        </div>

                        <div class="mb-3">
                            <label for="telepon">Telepon</label>
                            <input id="telepon" name="telepon" type="text" class="form-control" data-parsley-type="digits" required>
                        </div>
                        <div class="mb-3">
                            <label for="email">Email</label>
                            <input id="email" name="email" type="text" class="form-control"   data-parsley-type="email">
                        </div>
                        <div class="mb-3">
                            <label for="price">Website</label>
                            <input id="price" name="website" type="text" class="form-control">
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="alamat">Alamat</label>
                            <input id="alamat" name="alamat" type="text" class="form-control" required>
                        </div>
                        <div class="mb-3">
                            {{-- @php
                                $provinces = new App\Http\Controllers\DependentDropdownController;
                                $provinces = $provinces->provinces();
                            @endphp --}}
                            <label class="control-label">Propinsi</label>
                            <select class="form-control select2" name="provinsi" id="provinsi" required>
                                <option value="" selected disabled>-- Pilih Propinsi --</option>
                                @foreach ($provinces as $item)
                                    <option value="{{ $item->id ?? '' }}">{{ $item->name ?? '' }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="control-label">Kota/Kabupaten</label>
                            <select class="form-control select2" name="kota" id="kota" required>
                                <option value="">-- Pilih Kota/Kabupaten --</option>

                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="control-label">Kecamatan</label>
                            <select class="form-control select2" name="kecamatan" id="kecamatan" required>
                                <option value="">-- Pilih Kecamatan --</option>

                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="control-label">Kelurahan / Desa</label>
                            <select class="form-control select2" name="desa" id="desa" required>
                                <option value="">-- Pilih Desa --</option>

                            </select>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">

                <h4 class="card-title">Susunan Pengurus</h4>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="productname">Ketua</label>
                            <input id="productname" name="ketua" type="text" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="manufacturername">Sekretaris</label>
                            <input id="manufacturername" name="sekretaris" type="text" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="manufacturerbrand">Bendahara</label>
                            <input id="manufacturerbrand" name="bendahara" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="manufacturername">Ketua BKAD</label>
                            <input id="manufacturername" name="ketuaBkad" type="text" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="manufacturername">Ketua BPUPK</label>
                            <input id="manufacturername" name="ketuaBpupk" type="text" class="form-control">
                        </div>
                        <div class="mb-3 mt-2">
                            <label for="manufacturername">Logo</label>
                            <input name="fileGambar" type="file" class="form-control" placeholder="Choose image" id="image" accept="image/*" />
                            <p class="mt-2">

                                <img id="preview-image" src="{{ url('assets/images/upload.png') }}"
                                alt="preview image" style="max-height: 100px;">
                            </p>

                           <a class="btn btn-danger btn-sm" id="removePreview" style="display: none;">Hapus</a>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                        <button type="button" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
    <!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script type="text/javascript">
    $('#image').change(function(){

    let reader = new FileReader();
    reader.onload = (e) => {
      $('#preview-image').attr('src', e.target.result);
    }
    reader.readAsDataURL(this.files[0]);

   });

   $('#removePreview').click(function(){
       $('#image').val('');
       $('#preview-image').attr('src',"{{ url('assets/images/upload.png') }}");
   })

   function onChangeSelect(url, id, name) {
            // send ajax request to get the cities of the selected province and append to the select tag
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    id: id
                },
                success: function (data) {
                    $('#' + name).empty();
                    $('#' + name).append('<option selected disabled>-- Pilih Salah Satu --</option>');

                    $.each(data, function (key, value) {
                        $('#' + name).append('<option value="' + key + '">' + value + '</option>');
                    });
                }
            });
        }
        $(function () {
            $('#provinsi').on('change', function () {
                onChangeSelect('{{ route("cities") }}', $(this).val(), 'kota');
            });
            $('#kota').on('change', function () {
                onChangeSelect('{{ route("districts") }}', $(this).val(), 'kecamatan');
            })
            $('#kecamatan').on('change', function () {
                onChangeSelect('{{ route("villages") }}', $(this).val(), 'desa');
            })
        });
  </script>
@endsection

