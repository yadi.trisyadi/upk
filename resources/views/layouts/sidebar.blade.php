<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">Beranda</li>
                <li>
                    <a href="{{ url('/') }}" class="waves-effect">
                        <i class="bx bx-home"></i>
                        <span key="t-layouts">Dashboard</span>
                    </a>
                </li>

                <li class="menu-title" key="t-menu">Pengaturan</li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-id-card"></i>
                        <span key="t-layouts">Profil</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="{{ url('/pengaturan-profile') }}" key="t-light-sidebar">Identitas UPK</a></li>
                        <li><a href="{{ url('/pengaturan-kecamatan') }}" key="t-light-sidebar">Kecamatan</a></li>
                        <li><a href="{{ url('/pengaturan-desa') }}" key="t-light-sidebar">Desa</a></li>
                        <li><a href="{{ url('/pengaturan-pengurus') }}" key="t-light-sidebar">Pengurus</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bxs-calculator"></i>
                        <span key="t-layouts">Akuntansi</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Periode Akuntansi</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Saldo Awal</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Hibah</a></li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Tutup Buku</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">Backup Data</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">Validator</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Pembagian Surplus</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Perubahan Surplus</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Proses Tutup Buku</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Saldo Awal</a></li>
                                <li><a href="layouts-icon-sidebar"
                                    key="t-icon-sidebar">Neraca Awal</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-user-circle"></i>
                        <span key="t-layouts">Pengguna</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Pengguna</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Hak Akses Pengguna</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-list-ul"></i>
                        <span key="t-layouts">Data Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="{{ url('/master-jenis-kegiatan') }}" key="t-light-sidebar">Jenis Kegiatan</a></li>
                        <li><a href="{{ url('/master-jenis-usaha') }}" key="t-light-sidebar">Jenis Usaha</a></li>
                        <li><a href="{{ url('/master-kekeluargaan') }}" key="t-light-sidebar">Kekeluargaan</a></li>
                        <li><a href="{{ url('/master-pekerjaan') }}" key="t-light-sidebar">Pekerjaan</a></li>
                        <li><a href="{{ url('/master-agama') }}" key="t-light-sidebar">Agama</a></li>
                        <li><a href="{{ url('/master-status-perkawinan') }}" key="t-light-sidebar">Status Perkawinan</a></li>
                    </ul>
                </li>

                <li class="menu-title" key="t-menu">Manajemen</li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-file"></i>
                        <span key="t-layouts">Proposal</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="{{ url('proposal-kelompok') }}" key="t-light-sidebar">Kelompok</a></li>
                        <li><a href="{{ url('proposal-individu') }}" key="t-light-sidebar">Individu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-dollar-circle"></i>
                        <span key="t-layouts">Pinjaman</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Kelompok</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">SPP</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Lainnya</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Individu</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">SPP</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Perorangan</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="menu-title" key="t-menu">Keanggotaan</li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-users"></i>
                        <span key="t-layouts">Kelompok</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="{{ url('kelompok-list') }}" key="t-light-sidebar">Daftar Kelompok</a></li>
                        <li><a href="{{ url('kelompok-anggota') }}" key="t-light-sidebar">Anggota Kelompok</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('individu-anggota') }}" class="waves-effect">
                        <i class="fas fa-user"></i>
                        <span key="t-layouts">Individu</span>
                    </a>
                </li>

                <li class="menu-title" key="t-menu">Transaksi</li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-check-double"></i>
                        <span key="t-layouts">Penerimaan Angsuran</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Kelompok</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">SPP</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Lainnya</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Individu</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">SPP</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Perorangan</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-credit-card"></i>
                        <span key="t-layouts">Penyaluran Kredit</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Kelompok</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">SPP</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Individu</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">SPP</a></li>
                                <li><a href="layouts-icon-sidebar"
                                        key="t-icon-sidebar">Perorangan</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-book"></i>
                        <span key="t-layouts">Buku Kas</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">SPP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">UEP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">OPS</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Perorangan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-book"></i>
                        <span key="t-layouts">Buku Bank</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">SPP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">UEP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">OPS</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Perorangan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-book"></i>
                        <span key="t-layouts">Buku Bantu Lainnya</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar BDD</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Inventaris</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Aktiva Tetap</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Hutang</a></li>
                    </ul>
                </li>

                <li class="menu-title" key="t-menu">Laporan</li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-money"></i>
                        <span key="t-layouts">Laporan Keuangan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Neraca Mikrofinance</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Rugi Laba Mikrofinance</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-car"></i>
                        <span key="t-layouts">Operasional</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Kas Operasional</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Bank Operasional</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-data"></i>
                        <span key="t-layouts">UEP</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Kas UEP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Bank UEP</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-data"></i>
                        <span key="t-layouts">SPP</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Kas SPKP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Bank SPKP</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-user"></i>
                        <span key="t-layouts">Perorangan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Kas Perorangan</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Bank Perorangan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-users"></i>
                        <span key="t-layouts">Kelompok</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Kelompok SPKP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Kelompok UEP</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bxs-file-blank"></i>
                        <span key="t-layouts">Berkas</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Cover</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Isi</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Surat Pengantar</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Kata Pengantar</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Profil Pengurus UPK</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Pembatas Laporan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-book"></i>
                        <span key="t-layouts">Buku Pembantu</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar BDD</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Inventaris</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Aktiva Tetap</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Biaya Lain-Lain</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Perguliran SPP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Perguliran UEP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Daftar Pemb. IPTW</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bxs-archive-out"></i>
                        <span key="t-layouts">Pinjaman UEP</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Kelompok</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">LPP UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">Kolektibilitas UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">Penghapusan UEP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">LPP Lain-Lain</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Individu</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">LPP UEP Individu</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">Kolektibilitas UEP Individu</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bxs-archive-out"></i>
                        <span key="t-layouts">Pinjaman SPP</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Kelompok</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">LPP SPP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">Kolektibilitas SPKP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">Penghapusan SPKP</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">LPP Lain-Lain SPKP</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript: void(0);" class="has-arrow"
                                key="t-vertical">Individu</a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="layouts-light-sidebar"
                                        key="t-light-sidebar">LPP SPP Individu</a></li>
                                <li><a href="layouts-compact-sidebar"
                                        key="t-compact-sidebar">Kolektibilitas SPP Individu</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-user"></i>
                        <span key="t-layouts">Pinjaman Perorangan</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Perkembangan Lainnya</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Kolektibilitas Lainnya</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-users"></i>
                        <span key="t-layouts">Perkemb Kelompok</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Form 84a</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Form 84b</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Form 85c</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-folder-open"></i>
                        <span key="t-layouts">Rekap Pinjaman UEP</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Rekap LPP UEP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Rekap Kolekt UEP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Rekap Lain-Lain UEP</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-folder-open"></i>
                        <span key="t-layouts">Rekap Pinjaman SPP</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Rekap LPP SPP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Rekap Kolekt SPP</a></li>
                        <li><a href="layouts-light-sidebar" key="t-light-sidebar">Rekap Lain-Lain SPP</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
