@extends('layouts.master')

@section('title') @lang('translation.Crypto') @endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Beranda @endslot
        @slot('title') Dashboard @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div>
                    <div class="row">
                        <div class="col-lg-12 col-sm-8">
                            <div class="p-4">
                                <h5 class="text-primary">Selamat Datang</h5>
                                <p>Di Halaman Depan Aplikasi BUMDESMA</p>

                                <div class="text-muted">
                                    <p class="mb-1"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> 
                                        Sistem Informasi Manajemen Keuangan UPK</p>
                                    <p class="mb-1"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i> 
                                        Laporan Keuangan</p>
                                    <p class="mb-0"><i class="mdi mdi-circle-medium align-middle text-primary me-1"></i>
                                        Akuntansi Keuangan</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-4 align-self-center">
                            <div>
                                <img src="{{ URL::asset('/assets/images/crypto/features-img/img-1.png') }}" alt="" class="img-fluid d-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- end row -->

    

@endsection
@section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <!-- crypto dash init js -->
    <script src="{{ URL::asset('/assets/js/pages/crypto-dashboard.init.js') }}"></script>
@endsection