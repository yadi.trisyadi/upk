@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('/assets/libs/datepicker/datepicker.min.css') }}">
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Keanggotaan @endslot
@slot('title') Tambah Kelompok @endslot
@endcomponent

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ route('kelompok-list.store') }}" method="post" class="custom-validation">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jenisKegiatan">Jenis Kelompok</label>
                                <select name="jenisKelompok" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih Jenis Kelompok --</option>
                                    <option value="1">SPKP</option>
                                    <option value="2">UEP</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Kode Kelompok</label>
                                <input type="text" name="kodeKelompok" class="form-control" id="kodeKelompok" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Nama Kelompok</label>
                                <input type="text" name="namaKelompok" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="desa">Kelurahan/Desa</label>
                                <select name="desa" class="form-control select2">
                                    <option value="" selected disabled>--- Pilih Desa ---</option>
                                    @foreach ($desa as $desa)
                                    <option value="{{ $desa->kode }}">{{ $desa->nama_desa }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="alamat">Alamat Kelompok</label>
                                <textarea name="alamatKelompok" class="form-control"></textarea>
                            </div>
                            <div class="mb-3">
                                <label for="alamat">No Telepon</label>
                                <input type="text" name="telp" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="alamat">Nama Ketua</label>
                                <input type="text" name="namaKetua" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="alamat">Alamat Ketua</label>
                                <input type="text" name="alamatKetua" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="anggotaAwal">Anggota Awal</label>
                                <div class="input-group">
                                    <input type="number" name="anggotaAwal" class="form-control" value="0">
                                    <div class="input-group-text">Orang</div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="anggotaAwal">Anggota Sekarang</label>
                                <div class="input-group">
                                    <input type="number" name="anggotaSekarang" class="form-control" value="0">
                                    <div class="input-group-text">Orang</div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="tanggalBerdiri">Tanggal Berdiri</label>
                                <div class="input-group" id="datepicker2">
                                    <input type="text" class="form-control" placeholder="Tanggal Berdiri"
                                        data-date-format="yyyy-mm-dd" data-date-container='#datepicker2'
                                        data-provide="datepicker" data-date-autoclose="true" name="tanggalBerdiri">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="tanggalBerdiri">Tanggal Bergabung</label>
                                <div class="input-group" id="datepicker2">
                                    <input type="text" class="form-control" placeholder="Tanggal Bergabung"
                                        data-date-format="yyyy-mm-dd" data-date-container='#datepicker2'
                                        data-provide="datepicker" data-date-autoclose="true" name="tanggalBergabung">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="awalPinjaman">Awal Pinjaman</label>
                                <select name="awalPinjaman" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Reguler</option>
                                    <option value="2">Perguliran</option>
                                    <option value="3">Lain-lain</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="jenisUsaha">Jenis Usaha</label>
                                <select name="jenisUsaha" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    @foreach ($jenisUsaha as $jenisUsaha)
                                    <option value="{{ $jenisUsaha->id }}">{{ $jenisUsaha->jenis_usaha }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="jenisKegiatan">Jenis Kegiatan</label>
                                <select name="jenisKegiatan" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    @foreach ($jenisKegiatan as $jenisKegiatan)
                                    <option value="{{ $jenisKegiatan->id }}">{{ $jenisKegiatan->jenis_kegiatan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="jenisKegiatan">Fungsi</label>
                                <select name="fungsi" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Penyalur</option>
                                    <option value="2">Pengelola</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="jenisKegiatan">Tingkat</label>
                                <select name="tingkat" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="1">Pemula</option>
                                    <option value="2">Berkembang</option>
                                    <option value="3">Mandiri</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="jenisKegiatan">Status</label>
                                <select name="status" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="0">Aktif</option>
                                    <option value="1">Pasif</option>
                                </select>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap gap-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                            <button type="button" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datepicker/datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-advanced.init.js') }}"></script>


@endsection
