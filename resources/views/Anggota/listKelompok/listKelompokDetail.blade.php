@extends('layouts.master')

@section('title') @lang('translation.Profile') @endsection

@section('css')
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
        type="text/css">
        <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Keanggotaan @endslot
        @slot('title') Detail Kelompok @endslot
    @endcomponent
@foreach ($data as $data )

    <div class="row">
        <div class="col-xl-4">
            <div class="card overflow-hidden">
                <div class="bg-primary bg-soft">
                    <div class="row">
                        <div class="col-7">
                            <div class="text-primary p-3">
                                <h5 class="text-primary">{{ $data->nama_kelompok }}</h5>
                                <p>{{ $data->alamat_kelompok }},<br>{{ $data->telp_kelompok }}<br>
                                {{ namaDesaKelompok($data->desa) }} </p>
                            </div>
                        </div>
                        <div class="col-5 align-self-end">
                            <img src="{{ URL::asset('/assets/images/profile-img.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        {{--  <div class="col-sm-4">
                            <div class="avatar-md profile-user-wid mb-4">
                                <img src="{{ isset(Auth::user()->avatar) ? asset(Auth::user()->avatar) : asset('/assets/images/users/avatar-1.jpg') }}" alt="" class="img-thumbnail rounded-circle">
                            </div>
                            <h5 class="font-size-15 text-truncate">{{ Auth::user()->name }}</h5>
                            <p class="text-muted mb-0 text-truncate">UI/UX Designer</p>
                        </div>  --}}

                        <div class="col-sm-12">
                            <div class="pt-4">

                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="font-size-15">{{ $data->anggota_awal }} Orang</h5>
                                        <p class="text-muted mb-0">Anggota Awal</p>
                                    </div>
                                    <div class="col-6">
                                        <h5 class="font-size-15">{{ $data->anggota_sekarang }} Orang</h5>
                                        <p class="text-muted mb-0">Anggota Sekarang</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end card -->

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Informasi Lainnya</h4>


                    <div class="table-responsive">
                        <table class="table table-nowrap table-sm mb-0">
                            <tbody>
                                <tr>
                                    <th scope="row">Ketua</th>
                                    <td>:</td>
                                    <td>{{ $data->nama_ketua }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td>:</td>
                                    <td>{{ $data->alamat_ketua }}</td>
                                </tr>
                                <tr>
                                    <th>Tgl Berdiri</th>
                                    <td>:</td>
                                    <td>{{ $data->tanggal_berdiri }}</td>
                                </tr>
                                <tr>
                                    <th>Tgl Bergabung</th>
                                    <td>:</td>
                                    <td>{{ $data->tanggal_bergabung }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Usaha</th>
                                    <td>:</td>
                                    <td>{{ getNameJenisUsaha($data->jenis_usaha) }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kegiatan</th>
                                    <td>:</td>
                                    <td>{{ getNameJenisKegiatan($data->jenis_kegiatan) }}</td>
                                </tr>
                                <tr>
                                    <th>Fungsi</th>
                                    <td>:</td>
                                    <td>{{ getFungsi($data->fungsi) }}</td>
                                </tr>
                                <tr>
                                    <th>Tingkat</th>
                                    <td>:</td>
                                    <td>{{ getTingkat($data->tingkat) }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>:</td>
                                    <td>{{ getStatus($data->status) }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-8">

            <div class="row">
                <div class="col-md-4">
                    <div class="card mini-stats-wid">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-body">
                                    <p class="text-muted fw-medium mb-2">Pinjaman Lunas</p>
                                    <h5 class="mb-0">250.000.000</h5>
                                </div>

                                <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                    <span class="avatar-title">
                                        <i class="bx bx-check-circle font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mini-stats-wid">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-body">
                                    <p class="text-muted fw-medium mb-2">Pinjaman Aktif</p>
                                    <h5 class="mb-0">1.150.000.000</h5>
                                </div>

                                <div class="avatar-sm align-self-center mini-stat-icon rounded-circle bg-primary">
                                    <span class="avatar-title">
                                        <i class="bx bx-hourglass font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mini-stats-wid">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-body">
                                    <p class="text-muted fw-medium mb-2">Pinjaman Pasif</p>
                                    <h5 class="mb-0">50.000.000</h5>
                                </div>

                                <div class="avatar-sm align-self-center mini-stat-icon rounded-circle bg-primary">
                                    <span class="avatar-title">
                                        <i class="bx bx-package font-size-24"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Pinjaman Kelompok</h4>
                    <div class="table-responsive">
                        <table class="table table-nowrap table-hover mb-0 datatable">
                            <thead>
                                <tr>
                                    <th scope="col">Kode</th>
                                    <th scope="col">Tanggal Cair</th>
                                    <th scope="col">Pinjaman</th>
                                    <th scope="col">Jasa</th>
                                    <th scope="col">Waktu</th>
                                    <th scope="col">Pokok</th>
                                    <th scope="col">Bunga</th>
                                    <th scope="col">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>
                                <tr>
                                    <th scope="row">3210250510101</th>
                                    <td>10-10-2022</td>
                                    <td>50.000000</td>
                                    <td>20.00</td>
                                    <td>10</td>
                                    <td>5.000.000</td>
                                    <td>500.000</td>
                                    <td>Pasif</td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Anggota Kelompok</h4>
                <div class="table-responsive">
                    <table class="table table-nowrap table-hover mb-0 datatable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIK</th>
                                <th scope="col">Nama Lengkap</th>
                                <th scope="col">Jenis Kelamin</th>
                                <th scope="col">Jabatan</th>
                                <th scope="col">Jenis Usaha</th>
                                <th scope="col">Alamat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>32102547097900001</td>
                                <td>Yuyu Yuliani</td>
                                <td>Perempuan</td>
                                <td>Ketua</td>
                                <td>Dagang</td>
                                <td>Blok Sabtu</td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>32102547097900001</td>
                                <td>Yuyu Yuliani</td>
                                <td>Perempuan</td>
                                <td>Ketua</td>
                                <td>Dagang</td>
                                <td>Blok Sabtu</td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>32102547097900001</td>
                                <td>Yuyu Yuliani</td>
                                <td>Perempuan</td>
                                <td>Ketua</td>
                                <td>Dagang</td>
                                <td>Blok Sabtu</td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>32102547097900001</td>
                                <td>Yuyu Yuliani</td>
                                <td>Perempuan</td>
                                <td>Ketua</td>
                                <td>Dagang</td>
                                <td>Blok Sabtu</td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>32102547097900001</td>
                                <td>Yuyu Yuliani</td>
                                <td>Perempuan</td>
                                <td>Ketua</td>
                                <td>Dagang</td>
                                <td>Blok Sabtu</td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>32102547097900001</td>
                                <td>Yuyu Yuliani</td>
                                <td>Perempuan</td>
                                <td>Ketua</td>
                                <td>Dagang</td>
                                <td>Blok Sabtu</td>
                            </tr>
                            <tr>
                                <th scope="row">1</th>
                                <td>32102547097900001</td>
                                <td>Yuyu Yuliani</td>
                                <td>Perempuan</td>
                                <td>Ketua</td>
                                <td>Dagang</td>
                                <td>Blok Sabtu</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-primary" href="{{ url('kelompok-list') }}">Kembali</a>
            </div>
        </div>
    </div>
    <!-- end row -->



@endsection
@section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <!-- profile init -->
    <script src="{{ URL::asset('/assets/js/pages/profile.init.js') }}"></script>



@endsection
