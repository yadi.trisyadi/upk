@extends('layouts.master')

@section('title') @lang('translation.Profile') @endsection

@section('css')
    <link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
        type="text/css">
        <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Keanggotaan @endslot
        @slot('title') Detail Individu @endslot
    @endcomponent
@foreach ($data as $data )

    <div class="row">


            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Informasi Individu</h4>


                    <div class="table-responsive">
                        <table class="table table-nowrap table-sm mb-0">
                            <tbody>
                                <tr>
                                    <th scope="row">Tanggal Masuk</th>
                                    <td>:</td>
                                    <td>{{ $data->tanggal_masuk }}</td>
                                </tr>
                                <tr>
                                    <th>Kode Registrasi</th>
                                    <td>:</td>
                                    <td>{{ $data->kode_registrasi }}</td>
                                </tr>
                                <tr>
                                    <th>No Kartu Keluarga</th>
                                    <td>:</td>
                                    <td>{{ $data->no_kk }}</td>
                                </tr>
                                <tr>
                                    <th>No KTP</th>
                                    <td>:</td>
                                    <td>{{ $data->no_nik }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <td>:</td>
                                    <td>{{ $data->nama_lengkap }}</td>
                                </tr>
                                <tr>
                                    <th>Tempat, Tanggal Lahir</th>
                                    <td>:</td>
                                    <td>{{ $data->tempat_lahir }}, {{ $data->tanggal_lahir }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td>:</td>
                                    <td>{{ $data->alamat }}</td>
                                </tr>
                                <tr>
                                    <th>RT/RW</th>
                                    <td>:</td>
                                    <td>{{$data->rt }}/{{ $data->rw }}</td>
                                </tr>
                                <tr>
                                    <th>Desa/Kelurahan</th>
                                    <td>:</td>
                                    <td> {{ namaDesaKelompok($data->desa) }}</td>
                                </tr>
                                <tr>
                                    <th>Kecamatan</th>
                                    <td>:</td>
                                    <td>{{ $data->kecamatan }}</td>
                                </tr>
                                <tr>
                                    <th>Kabupaten</th>
                                    <td>:</td>
                                    <td>{{ $data->kabupaten }}</td>
                                </tr>
                                <tr>
                                    <th>Provinsi</th>
                                    <td>:</td>
                                    <td>{{ $data->propinsi }}</td>
                                </tr>
                                <tr>
                                    <th>Kode Pos</th>
                                    <td>:</td>
                                    <td>{{ $data->kode_pos }}</td>
                                </tr>
                                <tr>
                                    <th>No Handphone</th>
                                    <td>:</td>
                                    <td>{{ $data->telp }}</td>
                                </tr>
                                <tr>
                                    <th>Keanggotaan</th>
                                    <td>:</td>
                                    <td>{{ getKeanggotaan($data->keanggotaan) }}</td>
                                </tr>
                                <tr>
                                    <th>Status Keanggotaan</th>
                                    <td>:</td>
                                    <td>{{ getStatus($data->status) }}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Keluar</th>
                                    <td>:</td>
                                    <td>{{ $data->tanggal_keluar }}</td>
                                </tr>
                                <tr>
                                    <th>Agama</th>
                                    <td>:</td>
                                    <td>{{ getAgama($data->agama) }}</td>
                                </tr>
                                <tr>
                                    <th>Status Perkawinan</th>
                                    <td>:</td>
                                    <td>{{ getStatusPerkawinan($data->status_perkawinan) }}</td>
                                </tr>
                                <tr>
                                    <th>Pekerjaan</th>
                                    <td>:</td>
                                    <td>{{ getPekerjaan($data->pekerjaan) }}</td>
                                </tr>
                                <tr>
                                    <th>Jumlah Tabungan</th>
                                    <td>:</td>
                                    <td>Rp. {{ $data->jumlah_tabungan }}</td>
                                </tr>
                                <tr>
                                    <th>Jumlah Pinjaman</th>
                                    <td>:</td>
                                    <td>Rp. {{ $data->jumlah_pinjaman }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Usaha</th>
                                    <td>:</td>
                                    <td>{{ getNameJenisUsaha($data->jenis_usaha) }}</td>
                                </tr>
                                <tr>
                                    <th>Catatan Khusus</th>
                                    <td>:</td>
                                    <td>{{ $data->catatan_khusus }}</td>
                                </tr>

                            </tbody>
                        </table>
                        <br>
                        <a class="btn btn-primary" href="{{ url('individu-anggota') }}">Kembali</a>
                    </div>
                </div>
            </div>

            
        @endforeach
    </div>



@endsection
@section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <!-- profile init -->
    <script src="{{ URL::asset('/assets/js/pages/profile.init.js') }}"></script>



@endsection
