@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('/assets/libs/datepicker/datepicker.min.css') }}">
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
    type="text/css">

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Keanggotaan @endslot
@slot('title') Tambah Anggota Kelompok @endslot
@endcomponent

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ route('kelompok-anggota.store') }}" method="post" class="custom-validation" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jenisKegiatan">Nama Kelompok</label>
                                <select name="kelompok" class="form-control select2" required>
                                    <option value="" selected disabled>-- Pilih Kelompok --</option>
                                    @foreach ($kelompok as $kelompok )

                                    <option value="{{ $kelompok->id }}">{{ $kelompok->nama_kelompok }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Kode Anggota</label>
                                <input type="text" name="kodeAnggota" class="form-control" id="kodeAnggota" readonly>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">NIK</label>
                                <input type="text" name="nik" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="keterangan">Nama Lengkap</label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>
                            <div class="mb-3">
                                <label for="jenisKelamin">Jenis Kelamin</label>
                                <select name='jenisKelamin' class="form-control select2" required>
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="L">Laki-Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="desa">Jabatan</label>
                                <select name="jabatan" class="form-control select2">
                                    <option value="" selected disabled>--- Pilih Jabatan ---</option>
                                    @foreach ($jabatan as $jabatan)
                                    <option value="{{ $jabatan->id }}">{{ $jabatan->jabatan }}</option  ption>

                                    @endforeach

                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="alamat">Tempat Lahir</label>
                                <input type="text" name="tempatLahir" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="tanggalBerdiri">Tanggal Lahir</label>
                                <div class="input-group" id="datepicker2">
                                    <input type="text" class="form-control" placeholder="Tanggal Lahir"
                                        data-date-format="yyyy-mm-dd" data-date-container='#datepicker2'
                                        data-provide="datepicker" data-date-autoclose="true" name="tanggalLahir">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="alamat">No Telepon</label>
                                <input type="text" name="telp" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="alamat">Alamat </label>
                                <textarea name="alamat" class="form-control"></textarea>
                            </div>
                            <div class="mb-3">
                                <label for="status">Status</label>
                               <select name="status" class="form-control select2">
                                   <option value="" selected disabled>--- Pilih ---</option>
                                   <option value="0">Aktif</option>
                                   <option value="1">Pasif</option>
                               </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="alamat">Jumlah Tabungan</label>
                                <input type="text" name="jumlahTabungan" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="anggotaAwal">Pengajuan Proposal</label>
                                <input type="number" name="pengajuanProposal" class="form-control" value="0">
                            </div>
                            <div class="mb-3">
                                <label for="jenisUsaha">Jenis Usaha</label>
                                <select name="jenisUsaha" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    @foreach ($jenisUsaha as $jenisUsaha)
                                    <option value="{{ $jenisUsaha->id }}">{{ $jenisUsaha->jenis_usaha }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="tanggalBerdiri">Jamninan</label>
                                <input type="text" name="jaminan" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="tanggalBerdiri">Nama Penjamin</label>
                                <input type="text" name="namaPenjamin" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="tanggalBerdiri">Kekeluargaan</label>
                                <select name="kekeluargaan" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    @foreach ($kekeluargaan as $kekeluargaan)
                                    <option value="{{ $kekeluargaan->id }}">{{ $kekeluargaan->keluarga }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="awalPinjaman">Jenis Kelamin</label>
                                <select name="jenisKelaminPenjamin" class="form-control select2">
                                    <option value="" selected disabled>-- Pilih --</option>
                                    <option value="L">Laki-laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="mb-3 mt-2">
                                <label for="manufacturername">Photo</label>
                                <input name="fileGambar" type="file" class="form-control" placeholder="Choose image"
                                    id="image" accept="image/*" />
                                <p class="mt-2">

                                    <img id="preview-image" src="{{ url('assets/images/upload.png') }}"
                                        alt="preview image" style="max-height: 100px;">
                                </p>

                                <a class="btn btn-danger btn-sm" id="removePreview" style="display: none;">Hapus</a>
                            </div>

                        </div>
                        <div class="d-flex flex-wrap gap-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                            <button type="button" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datepicker/datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-advanced.init.js') }}"></script>
<script>
    $('#image').change(function () {

        let reader = new FileReader();
        reader.onload = (e) => {
            $('#preview-image').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);

    });

    $('#removePreview').click(function () {
        $('#image').val('');
        $('#preview-image').attr('src', "{{ url('assets/images/upload.png') }}");
    })

</script>

@endsection
