@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
    <!-- DataTables -->
    <link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    @component('components.breadcrumb')
        @slot('li_1') Keangotaan @endslot
        @slot('title') Daftar Kelompok @endslot
    @endcomponent

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                        <div>{{ $message }}</div>
                    </div>
	                @endif
                    {{-- <h4 class="card-title">Default Datatable</h4>
                    <p class="card-title-desc">DataTables has most features enabled by
                        default, so all you need to do to use it with your own tables is to call
                        the construction function: <code>$().DataTable();</code>.
                    </p> --}}
                    <p>


                        <a class="btn btn-primary btn-sm" href="{{ route('kelompok-anggota.create') }}">Tambah</a>
                    </p>
                    <table id="datatable" class="table table-bordered dt-responsive nowraps w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>JK</th>
                                <th>Alamat</th>
                                <th>Kelompok</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        @php
                            $i = 1;
                        @endphp
                        <tbody>
                            @foreach ($data as $data )
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $data->nik }}</td>
                                <td>{{ $data->nama_lengkap }}</td>
                                <td>{{ $data->jenis_kelamin }}</td>
                                <td>{{ $data->alamat }}</td>
                                <td>{{  getNamaKelompok($data->id_kelompok) }}</td>
                                <td>{{  getStatus($data->status) }}</td>
                                <td>

                                    <div class="dropdown float-end">
                                        <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="bx bxs-cog align-middle me-1"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end" style="">
                                            <form action="{{ route('kelompok-anggota.destroy',$data->id) }}" method="POST">
                                                <a class="dropdown-item" href="{{ route('kelompok-anggota.show',$data->id) }}">View</a>
                                                <a class="dropdown-item" href="{{ route('kelompok-anggota.edit',$data->id) }}">Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="dropdown-item">Delete</button>
                                                </form>
                                        </div>
                                    </div>



                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->



@endsection
@section('script')
    <!-- Required datatable js -->
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <!-- Datatable init js -->
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
