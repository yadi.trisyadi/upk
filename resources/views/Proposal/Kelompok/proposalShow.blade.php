@extends('layouts.master')

@section('title') {{ $title }} @endsection

@section('css')
<!-- DataTables -->
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Keangotaan @endslot
@slot('title') Daftar Proposal Kelompok @endslot
@endcomponent


<div class="col-xl-12">
    <div class="card">
        <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert border-0 border-start border-5 border-primary alert-dismissible fade show">
                <div>{{ $message }}</div>
            </div>
            @endif

            <p>
                <a class="btn btn-primary btn-sm" href="{{ route('proposal-kelompok.create') }}">Registrasi Proposal</a>
            </p>

            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#registrasi" role="tab">
                        <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                        <span class="d-none d-sm-block">Registrasi <span class="badge bg-danger rounded-pill">{{ $countRegistrasi }}</span> </span>

                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#verifikasi" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                        <span class="d-none d-sm-block">Verifikasi <span class="badge bg-danger rounded-pill">{{ $countVerifikasi }}</span> </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#penetapan" role="tab">
                        <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                        <span class="d-none d-sm-block">Penetapan <span class="badge bg-danger rounded-pill">{{ $countPenetapan }}</span></span>
                    </a>
                </li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content p-3 text-muted">
                <div class="tab-pane active" id="registrasi" role="tabpanel">
                    <p class="mb-0">
                        <table class="table table-bordered dt-responsive  nowrap w-100 datatable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Kode Registrasi</th>
                                    <th>Kelompok</th>
                                    <th>Desa</th>
                                    <th>Proposal</th>
                                    <th>Orang</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            @php
                            $i = 1;
                            @endphp
                            <tbody>
                                @foreach ($registrasi as $registrasi )
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $registrasi->tanggal_proposal }}</td>
                                    <td>{{ $registrasi->kode_registrasi }}</td>
                                    <td>{{ $registrasi->nama_kelompok }}</td>
                                    <td>{{ $registrasi->desa }}</td>
                                    <td align="right">Rp. {{ number_format($registrasi->jumlah_pengajuan) }}</td>
                                    <td>{{ $registrasi->jumlah_pemanfaat }}</td>
                                    <td>
                                       <a href="{{ url('proposal-detail/'.$registrasi->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Detail"> <i class="fas fa-tasks" ></i></a> &nbsp;
                                       <a href="{{ url('proposal-finished/'.$registrasi->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Print Dokumen"><i class="fas fa-print"></i></a> &nbsp;
                                       <a href="{{ url('verifikasi-proposal/'.$registrasi->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Verifikasi"><i class="fas fa-check-circle"></i></a>
                                        {{-- <form action="{{ route('kelompok-list.destroy',$data->id) }}"
                                        method="POST">
                                        <a class="btn btn-outline-primary btn-sm"
                                            href="{{ route('kelompok-list.show',$data->id) }}">View</a>
                                        <a class="btn btn-outline-primary btn-sm"
                                            href="{{ route('kelompok-list.edit',$data->id) }}">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                        </form> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </p>
                </div>
                <div class="tab-pane" id="verifikasi" role="tabpanel">
                    <p class="mb-0">
                        <table class="table table-bordered dt-responsive  nowrap w-100 datatable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Kode Registrasi</th>
                                    <th>Kelompok</th>
                                    <th>Desa</th>
                                    <th>Proposal</th>
                                    <th>Rekomendasi</th>
                                    <th>Orang</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            @php
                            $i = 1;
                            @endphp
                            <tbody>
                                @foreach ($verifikasi as $verifikasi )
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $verifikasi->tanggal_proposal }}</td>
                                    <td>{{ $verifikasi->kode_registrasi }}</td>
                                    <td>{{ $verifikasi->nama_kelompok }}</td>
                                    <td>{{ $verifikasi->desa }}</td>
                                    <td align="right">{{ number_format($verifikasi->jumlah_pengajuan) }}</td>
                                    <td align="right">{{ number_format($verifikasi->jumlah_rekomendasi) }}</td>
                                    <td>{{ $verifikasi->jumlah_pemanfaat }}</td>
                                    <td>
                                       <a href="{{ url('proposal-detail/'.$verifikasi->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Detail"> <i class="fas fa-tasks" ></i></a> &nbsp;
                                       <a href="" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Print Dokumen"><i class="fas fa-print"></i></a> &nbsp;
                                       <a href="" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Penetapan"><i class="fas fa-check-double"></i></a>
                                        {{-- <form action="{{ route('kelompok-list.destroy',$data->id) }}"
                                        method="POST">
                                        <a class="btn btn-outline-primary btn-sm"
                                            href="{{ route('kelompok-list.show',$data->id) }}">View</a>
                                        <a class="btn btn-outline-primary btn-sm"
                                            href="{{ route('kelompok-list.edit',$data->id) }}">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                        </form> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </p>
                </div>
                <div class="tab-pane" id="penetapan" role="tabpanel">
                    <p class="mb-0">
                        <table class="table table-bordered dt-responsive  nowrap w-100 datatable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Kode Registrasi</th>
                                    <th>Kelompok</th>
                                    <th>Desa</th>
                                    <th>Proposal</th>
                                    <th>Rekomendasi</th>
                                    <th>Orang</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            @php
                            $i = 1;
                            @endphp
                            <tbody>
                                @foreach ($penetapan as $penetapan )
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $penetapan->tanggal_proposal }}</td>
                                    <td>{{ $penetapan->kode_registrasi }}</td>
                                    <td>{{ $penetapan->nama_kelompok }}</td>
                                    <td>{{ $penetapan->desa }}</td>
                                    <td align="right">{{ number_format($penetapan->jumlah_pengajuan) }}</td>
                                    <td align="right">{{ number_format($penetapan->jumlah_rekomendasi) }}</td>
                                    <td>{{ $penetapan->jumlah_pemanfaat }}</td>
                                    <td>
                                        <a href="{{ url('proposal-detail/'.$penetapan->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Detail"> <i class="fas fa-tasks" ></i></a> &nbsp;
                                        <a href="" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Print Dokumen"><i class="fas fa-print"></i></a> &nbsp;
                                        {{-- <form action="{{ route('kelompok-list.destroy',$data->id) }}"
                                        method="POST">
                                        <a class="btn btn-outline-primary btn-sm"
                                            href="{{ route('kelompok-list.show',$data->id) }}">View</a>
                                        <a class="btn btn-outline-primary btn-sm"
                                            href="{{ route('kelompok-list.edit',$data->id) }}">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                        </form> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </p>
                </div>
            </div>
    </div>
</div>

@endsection
@section('script')
<!-- Required datatable js -->
<script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/jszip/jszip.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/pdfmake/pdfmake.min.js') }}"></script>
<!-- Datatable init js -->
<script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
@endsection
