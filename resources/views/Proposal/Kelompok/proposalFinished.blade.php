@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
    type="text/css">
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Proposal @endslot
@slot('title') Proposal Kelompok @endslot
@endcomponent
@foreach ($data as $data )

<div class="row">
    <div class="col-xl-4">
        <div class="card overflow-hidden">
            <div class="bg-primary bg-soft">
                <div class="row">
                    <div class="col-7">
                        <div class="text-primary p-3">
                            <h5 class="text-primary">{{ $data->nama_kelompok }}</h5>
                            <p>{{ $data->alamat_kelompok }},<br>{{ $data->telp_kelompok }}<br>
                                {{ namaDesaKelompok($data->desa) }} </p>
                        </div>
                    </div>
                    <div class="col-5 align-self-end">
                        <img src="{{ URL::asset('/assets/images/profile-img.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">



                </div>
            </div>
        </div>
        <!-- end card -->


    </div>

    <div class="col-xl-8">


            <div class="alert alert-success">
                <h4 class="alert-heading">Selamat!</h4>
                <p>Proses Registrasi Proposal Baru Berhasil</p>
                <hr>
                <p class="mb-0">Silahkan cetak dokumen proposal dibawah ini</p>
            </div>

    </div>

    <div class="row">
        <div class="card">
            <div class="card-body">
               <table class="table">
                   <tr>
                       <td><i class="far fa-file-alt"></i>  <a href="{{ url('dokumen-verifikasi/'.request()->id) }}">Lembar Verifikasi</a></td>
                       <td><i class="far fa-file-alt"></i> Daftar Anggota Pemanfaat</td>
                   </tr>
                   <tr>
                       <td><i class="far fa-file-alt"></i> Sampul Berkas</td>
                       <td><i class="far fa-file-alt"></i> Rencana Angsuran Kelompok</td>
                   </tr>
                   <tr>
                       <td><i class="far fa-file-alt"></i><a href="{{ url('dokumen-surat-rekomendasi/'.request()->id) }}"> Surat Rekomendasi</a> </td>
                       <td><i class="far fa-file-alt"></i> Pernyataan Tanggung Renteng</td>
                   </tr>
                   <tr>
                       <td><i class="far fa-file-alt"></i><a href="{{ url('dokumen-permohonan-kredit/'.request()->id) }}"> Surat Permohonan Kredit </a></td>
                       <td><i class="far fa-file-alt"></i> Pernyataan Jaminan Anggota</td>
                   </tr>
                   <tr>
                       <td><i class="far fa-file-alt"></i><a href="{{ url('dokumen-anggota-kelompok/'.request()->id) }}"> Daftar Anggota Kelompok</a> </td>
                       <td><i class="far fa-file-alt"></i> Pernyataan Penjamin</td>
                   </tr>
               </table>
            </div>
        </div>
    </div>
    @endforeach

    <!-- end row -->



    @endsection
    @section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <!-- profile init -->
    <script src="{{ URL::asset('/assets/js/pages/profile.init.js') }}"></script>

    <script>
        $("#proposalAnggota").on('input', '.jumlahProposal', function () {
            var calculated_total_sum = 0;

            $("#proposalAnggota .jumlahProposal").each(function () {
                var get_textbox_value = $(this).val();
                if ($.isNumeric(get_textbox_value)) {
                    calculated_total_sum += parseFloat(get_textbox_value);
                }
            });
            var totalProposal = parseInt(calculated_total_sum).toLocaleString();
            $("#totalProposal").html(totalProposal);
            $('#grandTotalProposal').val(calculated_total_sum);
        });

    </script>

    @endsection
