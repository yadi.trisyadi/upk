@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<!-- select2 css -->
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<!-- dropzone css -->
<link href="{{ url('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ URL::asset('/assets/libs/datepicker/datepicker.min.css') }}">
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
    type="text/css">
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Registrasi @endslot
@slot('title') Registrasi Proposal @endslot
@endcomponent
<form action="{{ url('store-proposal') }}"  method="post" class="custom-validation">
    @csrf
   <input type="hidden" name="idKelompok" value="{{ $idKelompok }}">
    @foreach ($data as $data)
        <input type="hidden" value="{{ $data['idAnggota'] }}" name="idAnggota[]">
        <input type="hidden" value="{{ $data['jumlahProposal'] }}" name="jumlahProposal[]">
    @endforeach
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-body">

                @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="nama">Tanggal</label>
                            <div class="input-group" id="datepicker2">
                                <input type="text" class="form-control" placeholder="Tanggal Proposal"
                                    data-date-format="yyyy-mm-dd" data-date-container='#datepicker2'
                                    data-provide="datepicker" data-date-autoclose="true" name="tanggal">
                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="alamat">Kode Registrasi</label>
                            <input id="kodeRegistrasi" name="kodeRegistrasi" type="text" class="form-control form-control-sm">
                        </div>
                        <div class="mb-3">
                            <label for="telp">Jumlah Pengajuan</label>
                            <input id="jumlahPengajuan" name="jumlahPengajuan" type="text" class="form-control form-control-sm" value="{{ $jumlahPengajuan }}" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="telp">Jumlah Pemanfaat</label>
                            <input id="jumlahPemanfaat" name="jumlahPemanfaat" type="text" class="form-control form-control-sm" value="{{ $jumlahPemanfaat }}" readonly>
                        </div>
                        <div class="mb-3">
                            <label>Angsuran Perbulan</label>
                            <div class="row">
                                <div class="col-4">
                                    <div>
                                        <p class="text-muted text-truncate mb-2">Pokok</p>
                                        <h5 class="mb-0" id="pokok"></h5>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div>
                                        <p class="text-muted text-truncate mb-2">Bunga</p>
                                        <h5 class="mb-0" id="angsuran"></h5>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div>
                                        <p class="text-muted text-truncate mb-2">Total Angsuran</p>
                                        <h5 class="mb-0" id="total"></h5>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="mb-3">
                            <label for="kepalaDesa">Nomor Proposal</label>
                            <input id="noProposal" name="noProposal" type="text" class="form-control form-control-sm">
                        </div>
                        <div class="mb-3">
                            <label for="kepalaDesa">Nomor Rekom Kades</label>
                            <input id="noRekomKades" name="noRekomKades" type="text" class="form-control form-control-sm">
                        </div>
                        <div class="mb-3">
                            <label for="kepalaDesa">Jasa</label>
                            <input id="jasa" name="jasa" type="text" class="form-control form-control-sm" value="20">
                        </div>
                        <div class="mb-3">
                            <label for="kepalaDesa">Jangka</label>
                            <input id="jangkaWaktu" name="jangkaWaktu" type="text" class="form-control form-control-sm" value="12">
                        </div>
                        <div class="mb-3">
                            <label for="status">Sistem Angsuran</label>
                            <select name="sistemAngsuran" class="form-control select2" required>
                                <option value="0">Bulanan</option>
                                <option value="2">2 Bulanan</option>
                                <option value="3">3 Bulanan</option>
                                <option value="4">4 Bulanan</option>
                                <option value="5">5 Bulanan</option>
                                <option value="6">6 Bulanan</option>
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap gap-2">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save Changes</button>
                        <button type="button" class="btn btn-secondary waves-effect waves-light">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
    <!-- end row -->

@endsection
@section('script')


<!-- select 2 plugin -->
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>

<!-- dropzone plugin -->
<script src="{{ url('assets/libs/dropzone/dropzone.min.js') }}"></script>

<!-- init js -->
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datepicker/datepicker.min.js') }}"></script>
<script type="text/javascript">
   $( document ).ready(function() {

        // var sukuBunga = sukuBunga / 100;
        // var pokok = jumlahPinjaman / jangkaWaktu;
        // var bunga = (jumlahPinjaman * sukuBunga) / jangkaWaktu;
        // var sisaPinjaman = jumlahPinjaman;
        // var jumlahAngsuran = parseInt(pokok) + parseInt(bunga);

        var jumlahPinjaman = $('#jumlahPengajuan').val();
        var sukuBunga = $('#jasa').val();
        var jangkaWaktu = $('#jangkaWaktu').val();

        function bunga(jumlahPinjaman,sukuBunga,jangkaWaktu){
            var sukuBunga = sukuBunga / 100;
            var angsuran = (jumlahPinjaman * sukuBunga) / 12;
            return parseFloat(angsuran);
        }

        function pokok(jumlahPinjaman, jangkaWaktu){
            var pokok = jumlahPinjaman / jangkaWaktu;
            return parseFloat(pokok);
        }

        function total(){
            var bungas =  bunga(jumlahPinjaman,sukuBunga,jangkaWaktu);
            var pokoks =  pokok(jumlahPinjaman, jangkaWaktu);
            return parseFloat(pokoks) + parseFloat(bungas) ;
        }

        $('#pokok').html(parseInt(pokok(jumlahPinjaman, jangkaWaktu)).toLocaleString());
        $('#angsuran').html(parseInt(bunga(jumlahPinjaman,sukuBunga,jangkaWaktu)).toLocaleString());
        $('#total').html(parseFloat(total(pokok, angsuran)).toLocaleString());

       $('#jasa').keyup(function(){
            var jumlahPinjaman = $('#jumlahPengajuan').val();
            var sukuBunga = $('#jasa').val();
            var jangkaWaktu = $('#jangkaWaktu').val();
            var bungas =  bunga(jumlahPinjaman,sukuBunga,jangkaWaktu);
            var pokoks =  pokok(jumlahPinjaman, jangkaWaktu);
            $('#pokok').html(parseInt(pokok(jumlahPinjaman, jangkaWaktu)).toLocaleString());
            $('#angsuran').html(parseInt(bunga(jumlahPinjaman,sukuBunga,jangkaWaktu)).toLocaleString());
            var total = parseFloat(pokoks) + parseFloat(bungas)
            $('#total').html(parseInt(total).toLocaleString());

        })

       $('#jangkaWaktu').keyup(function(){
            var jumlahPinjaman = $('#jumlahPengajuan').val();
            var sukuBunga = $('#jasa').val();
            var jangkaWaktu = $('#jangkaWaktu').val();
            var bungas =  bunga(jumlahPinjaman,sukuBunga,jangkaWaktu);
            var pokoks =  pokok(jumlahPinjaman, jangkaWaktu);
            $('#pokok').html(parseInt(pokok(jumlahPinjaman, jangkaWaktu)).toLocaleString());
            $('#angsuran').html(parseInt(bunga(jumlahPinjaman,sukuBunga,jangkaWaktu)).toLocaleString());
            var total = parseFloat(pokoks) + parseFloat(bungas);
            $('#total').html(parseInt(total).toLocaleString());
        })
    });


</script>

@endsection

