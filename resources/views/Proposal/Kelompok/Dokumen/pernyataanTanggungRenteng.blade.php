<html>
<head>
<title>Cetak Surat Pernyataan Tanggung Renteng</title>
</head>
<body>
	<table style="font-family: arial;" width="700">
		<tr>
			<td>
				<table>
					<tr>
						<td width="450" align="left">KELOMPOK ANGGREK</td>
						<td width="450" align="right">Lampiran 5</td>
					</tr>
					<tr>
						<td width="450" align="left" style="font-size: 13px;">Alamat : DESA GARAWASTU</td>
						<td width="450" align="right" style="font-size: 13px;">Dokumen Proposal Kredit</td>
					</tr>
				</Table><Table>
					<tr>
						<td style="font-size: 2px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="font-size: 25px;"><b>SURAT PERNYATAAN TANGGUNG RENTENG</b></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table style="font-size: 13px;">
					<tr>
						<td align="justify">
						Yang bertandatangan dibawah ini, kami seluruh anggota kelompok ANGGREK yang beralamat di Desa Garawastu 
						Kecamatan Sindang, masing-masing dalam kedudukan sebagai pribadi anggota kelompok, secara sadar dan 
						penuh tanggungjawab menyatakan kesanggupan untuk saling menanggung kewajiban dari anggota kelompok yang tidak dapat membayar angsuran pinjaman kepada UPK dengan ketentuan sebagai berikut:
						</td>
					</tr>
				</table>
				<br>
				<table style="font-size: 13px;">
					<tr>
						<td width="30px;"></td>
						<td width="20px" align="center" valign=top>1.</td>
						<td width="650px;" align="justify">Memberikan kewenangan kepada UPK dan Pengurus Kelompok untuk membekukan dan mencairkan
							tabungan para anggota kelompok yang menunggak sebesar kekurangan pokok dan bunga pada saat jatuh tempo angsuran;</td>
					</tr>
					<tr>
						<td width="30px;"></td>
						<td width="20px" align="center" valign=top>2.</td>
						<td width="650px;" align="justify">Memberikan kewenangan kepada UPK dan pengurus kelompok untuk membekukan dan membayarkan 
							seluruh tabungan kelompok guna menutupi sisa kewajiban pokok dan bunga kredit yang telah jatuh 
							tempo pelunasannya;</td>
					</tr>
					<tr>
						<td width="30px;"></td>
						<td width="20px" align="center" valign=top>3.</td>
						<td width="650px;" align="justify">Apabila tabungan kelompok pada poin 1 dan 2 tidak mencukupi untuk melunasi sisa pokok dan bunga 
							kredit yang telah jatuh tempo pelunasannya, maka: <br>
						<table style="font-size: 13px;">
							<tr>
								<td width="15px" align="left" valign=top>a.</td>
								<td align="justify">Ketua atau pengurus kelompok akan menjual jaminan para anggota yang tidak memenuhi 
									kewajiban tersebut dan akan memperhitungkan hasilnya untuk melunasi sisa pokok dan bunga 
									kredit. Kelebihan dari jumlah tersebut akan dikembalikan kepada masing-masing yang 
									bersangkutan;</td>
							</tr>
							<tr>
								<td width="15px" align="left" valign=top>b.</td>
								<td align="justify">Apabila anggota penunggak tidak memiliki jaminan atau hasil penjualan tersebut tidak mencukupi 
									untuk melunasi sisa pokok dan bunga kredit, maka setiap anggota kelompok diwajibkan untuk 
									membayar sama banyaknya sisa kewajiban tersebut. Ketua atau pengurus kelompok bertanggung 
									jawab atas kelancaran penyetoran ini sesuai dengan batas waktu yang telah disepakati dengan 
									pihak UPK.</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				<br>
				<table style="font-size: 13px;">
					<tr>
						<td>Demikian surat pernyataan kesanggupan tanggung renteng ini dibuat secara sadar dan penuh tanggung jawab 
							untuk dipergunakan sebagaimana mestinya.</td>
					</tr>
				</table>
				<br>
				<table style="font-size: 13px;">
					<tr>
						<td width="650" align="left" >Anggota Kelompok ANGGREK</td>
						<td width="650" align="right">Garawastu, 19 Nopember 2018</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
				</table>
				<table border="2" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px;">
					<tr height="40px">
						<td style="width: 30px;" align="center"><b>No</b></td>
						<td style="width: 170px;" align="center"><b>Nama Lengkap</b></td>
						<td style="width: 160px;" align="center"><b>Alamat</b></td>
						<td style="width: 140px;" align="center"><b>Jaminan</b></td>
						<td style="width: 130px;" align="center"><b>Saldo Tabungan</b></td>
						<td style="width: 70px;" align="center"><b>Tanda Tangan</b></td>
					</tr>
					<tr height="30px">
						<td style="width: 30px;" align="center">1 </td>
						<td style="width: 170px;" align="left"><b>ROENAH</b><br>NIK : 3210084910710001</td>
						<td style="width: 160px;" align="left">BLOK SELASA</td>
						<td style="width: 140px;" align="left">BPKB</td>
						<td style="width: 130px;" align="right"> 3,300,000</td>
						<td style="width: 70px;" align="left">1</td>
					</tr>
				</table>
				<table>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">Mengetahui,</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">Kepala Desa</td>
					<td width="350" align="center">Ketua Kelompok</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center"><b><u>DEDI SETIADI</u></b></td>
					<td width="350" align="center"><b><u>ROENAH</u></b></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</body>
</html>