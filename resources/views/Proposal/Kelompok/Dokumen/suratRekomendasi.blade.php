<html>
<head>
<title>Cetak Surat Rekomendasi</title>
</head>
<body>
    <center>
        <h3>PEMERINTAH KABUPATEN MAJALENGKA<br>Kecamatan Sindang<br>Kantor Kepala Desa GARAWATU</h3></center>

    </center>

<table style="font-family: arial; font-size: 14px;">
	<tr>
		<td>
			<table style="width: 720px">
				<tr>
					<td style="border-bottom: 5px solid #000; " align="center"></td>
				</tr>
				<tr>
					<td style="border-top: 2px solid #000; " align="center"></td>
				</tr>
				{{-- <tr>
					<td width="700" align="center">&nbsp;</td>
				</tr> --}}
				<tr>
					<td align="center"><span style="font-size: 14;"><b><u>SURAT REKOMENDASI</u></b></span>
                        <br>Nomor :
                    </td>
				</tr>

				<tr>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="left">Yang bertanda tangan dibawah ini :</td>
				</tr>
				<tr>
					<td  align="center"></td>
				</tr>
			</table>
			<table border="0">
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Nama</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left"><b>DEDI SETIADI</b></td>
				</tr>
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Jabatan</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left">Kepala Desa Garawastu</td>
				</tr>
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Alamat</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left">Jalan Raya Desa Garawastu Kec. Sindang</td>
				</tr>
			</table>
			<table>
			<tr>
					<td width="700" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="700" align="left">Dengan ini menerangkan kepada :</td>
				</tr>
				<tr>
					<td width="700" align="center"></td>
				</tr>
			</table>
			<table border="0">
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Nama</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left"><b>ROENAH</b></td>
				</tr>
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Ketua kelompok</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left">ANGGREK</td>
				</tr>
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Alamat</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left">Kepala Desa Garawastu</td>
				</tr>
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Jumlah Pemanfaat</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left">9 Orang</td>
				</tr>
				<tr>
					<td style="width: 50px;" align="left"></td>
					<td style="width: 170px;" align="left">Jenis Usaha</td>
					<td style="width: 20px;" align="left">:</td>
					<td style="width: 450px;" align="left">SIMPAN PINJAM</td>
				</tr>
			</table>
			<table style="width: 720px">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td  align="justify"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Adalah benar-benar pengurus dan anggota kelompok SPKP yang beralamat di Desa GARAWASTU, Kecamatan SINDANG serta aktif menjalankan usaha/kegiatan sebagaimana tersebut diatas. Atas usaha/kegiatan dimaksud, maka dengan ini direkomendasikan untuk memperoleh pinjaman berdasarkan ketentuan dan aturan yang berlaku.
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="justify"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Demikian Surat Rekomendasi ini dibuat dengan sebenar-benarnya, untuk dipergunakan seperlunya.
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Garawastu, 11 Desember 2022</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Kepala Desa Garawastu</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center"><b><u>DEDI SETIADI</u></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
