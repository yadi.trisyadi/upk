<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cetak Daftar Anggota Pemanfaat</title>
</head>
<body>
	<table style="font-family: arial;">
		<tr>
			<td>
				<table>
					<tr>
						<td width="450" align="left">KELOMPOK ANGGREK</td>
						<td width="450" align="right">Lampiran 2</td>
					</tr>
					<tr>
						<td width="450" align="left" style="font-size: 13px;">Alamat : DESA GARAWASTU</td>
						<td width="450" align="right" style="font-size: 13px;">Dokumen Proposal Kredit</td>
					</tr>
				</Table><Table>
					<tr>
						<td style="font-size: 2px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="font-size: 25px;"><b>DAFTAR ANGGOTA PEMANFAAT PINJAMAN</b></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="width: 200px;">ID Kelompok</td>
						<td style="width: 350px;">: &nbsp;201272312873872</td>
						<td style="width: 200px;">Permohonan</td>
						<td style="width: 300px;">: &nbsp;Rp. 33,000,000</td>
					</tr>
					<tr>
						<td style="width: 200px;">Nama Kelompok</td>
						<td style="width: 350px;">: &nbsp;ANGGREK</td>
						<td style="width: 200px;">Jasa</td>
						<td style="width: 300px;">: &nbsp;20.00% per tahun</td>
					</tr>
					<tr>
						<td style="width: 200px;">Desa</td>
						<td style="width: 350px;">: &nbsp;GARAWASTU</td>
						<td style="width: 200px;">Jangka Waktu</td>
						<td style="width: 300px;">: &nbsp;12 Bulan</td>
					</tr>
					<tr>
						<td style="width: 200px;">Pemanfaat</td>
						<td style="width: 350px;">: &nbsp;9 Orang</td>
						<td style="width: 200px;">Periodisasi</td>
						<td style="width: 300px;">: &nbsp;Satu Kali Setiap Bulan</td>
					</tr>

				</table>
				<br>
				<table>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
				</table>
				<table border="2" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px;">
					<tr>
						<td style="width: 20px;" align="center"><b>No.</b></td>
						<td style="width: 200px;" align="center"><b>Nama Lengkap</b></td>
						<td style="width: 165px;" align="center"><b>Tempat & Tanggal Lahir</b></td>
						<td style="width: 130px;" align="center"><b>Jenis Usaha</b></td>
						<td style="width: 85px;" align="center"><b>Penganjuan Kredit</b></td>
						<td style="width: 70px;" align="center"><b>Tanda Tangan</b></td>
					</tr>
					<tr>
						<td style="width: 20px;" align="center">1. </td>
						<td style="width: 200px;" align="LEFT"><b>ROENAH</b> <br> ID:32102503105001</td>
						<td style="width: 165px;" align="center">MAJALENGKA, 12-11-1990</td>
						<td style="width: 130px;" align="center">Perdagangan</td>
						<td style="width: 85px;" align="right">10,000,000</td>
						<td style="width: 70px;" align="left">&nbsp;&nbsp;1</td>
					</tr>
				</table>
				<table>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Garawastu, 11 Desember 2022</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Ketua Kelompok</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center"><b><u>ROENAH</u></b></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</body>
</html>