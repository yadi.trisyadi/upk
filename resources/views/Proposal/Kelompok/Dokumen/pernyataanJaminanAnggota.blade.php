<html>
<head>
<title>Cetak Surat Pernyataan Jaminan Anggota</title>
</head>
<body>
	<table style="font-family: arial;" width="700">
		<tr>
			<td>
				<table>
					<tr>
						<td width="450" align="left">KELOMPOK ANGGREK</td>
						<td width="450" align="right">Lampiran 6.a</td>
					</tr>
					<tr>
						<td width="450" align="left" style="font-size: 13px;">Alamat : DESA GARAWASTU</td>
						<td width="450" align="right" style="font-size: 13px;">Dokumen Proposal Kredit</td>
					</tr>
				</Table><Table>
					<tr>
						<td style="font-size: 2px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td style="font-size: 2px;">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="font-size: 25px;"><b>SURAT PERNYATAAN</b></td>
					</tr>
					<tr>
						<td align="center" style="font-size: 25px;"><b>ANGGOTA PEMANFAAT PINJAMAN</b></td>
					</tr>
					<tr>
						<td style="font-size: 2px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table width="700px" style="font-size: 13px;">
					<tr> 
						<td>Yang bertanda tangan dibawah ini:</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table style="font-size: 13px;">
					<tr>
						<td width="170" >Nama Lengkap</td>
						<td width="10" >:</td>
						<td><b>NUNUNG NURJANNAH</b></td>
					</tr>
					<tr>
						<td width="170" >Tempat, Tanggal Lahir</td>
						<td width="10" >:</td>
						<td>MAJALENGKA, 12-11-1976</td>
					</tr>
					<tr>
						<td width="170" >Jenis Kelamin</td>
						<td width="10" >:</td>
						<td>PEREMPUAN</td>
					</tr>
					<tr>
						<td width="170" >NIK</td>
						<td width="10" >:</td>
						<td>3210085211760041</td>
					</tr>
					<tr>
						<td width="170" >Alamat</td>
						<td width="10" >:</td>
						<td>BLOK RABU</td>
					</tr>
				</table>
				<table style="font-size: 13px;">
					<tr>
						<td>Dengan ini menyatakan dengan sebenarnya, bahwa:</td>
					</tr>
				</table>
				<table style="font-size: 13px;">
					<tr style="font-size: 4px;">
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top>1.</td>
						<td>Saya selaku Anggota Kelompok Anggrek binaan UPK Kecamatan Sindang melalui Desa/Kelurahan Garawastu, 
							Kecamatan Sindang Kabupaten Majalengka, benar-benar telah meminjam uang sebesar :</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>Rp. ______________________, Terbilang ( ________________________________________________ )</td>
					</tr>
					<tr style="font-size: 4px;">
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top>2.</td>
						<td >Terhadap pinjaman tersebut diatas, berikut saya jaminkan barang berupa: <br>
							a. Jenis Barang : _______________________________, Nilai Jual Rp. : _________________________ <br>
							b. Jenis Barang : _______________________________, Nilai Jual Rp. : _________________________ <br>
							c. Jenis Barang : _______________________________, Nilai Jual Rp. : _________________________
						</td>
					</tr>
					<tr style="font-size: 4px;">
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top>3.</td>
						<td>Barang yang saya jaminkan kepada kelompok tersebut adalah benarbenar milik saya sendiri;</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top>4.</td>
						<td>Saya berkewajiban merawat dan melindungi barang jaminan tersebut dan tidak akan menjual, menggadaikan, dan/atau memindahtangankan kepada pihak lain sebelum kredit/pinjaman saya tersebut lunas;</td>
					</tr>
					<tr style="font-size: 4px;">
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top>5.</td>
						<td>Apabila terjadi kemacetan atas kredit saya tersebut, saya bersedia menyerahkan barang jaminan tersebut kepada pihak yang berwenang, guna menyelesaikan kredit/pinjaman saya kepada UPK Kecamatan Sindang;</td>
					</tr>
					<tr style="font-size: 4px;">
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top>6.</td>
						<td>Saya berjanji akan mengembalikan pinjaman saya tersebut sesuai dengan peraturan yang ada di UPK Kecamatan Sindang;</td>
					</tr>
					<tr style="font-size: 4px;">
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top>7.</td>
						<td>Apabila di kemudian hari saya melanggar isi dari surat pernyataan ini, maka saya bersedia dilaporkan kepada pihak yang berwajib dan/atau diproses secara hukum.</td>
					</tr>
					<tr>
						<td width="30"></td>
						<td width="20" align="center" valign=top></td>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table style="font-size: 13px;">
					<tr>
						<td>Demikian surat pernyataan ini saya buat dengan sebenarnya dan dengan penuh kesadaran serta rasa tanggung jawab</td>
					</tr>
				</table>
				<br>
				<br>
				<table border="0" style="font-size: 13px;">
					<tr>
						<td style="width: 20;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">&nbsp;</td>
						<td style="width: 280;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">Garawastu, 19 Nopember 2018</td>
					</tr>
					<tr>
						<td style="width: 20;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">Saksi 1</td>
						<td style="width: 280;" align="center">Saksi 2</td>
						<td style="width: 200;" align="center">Yang Menyatakan</td>
					</tr>
					<tr>
						<td style="width: 50;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">Penjamin</td>
						<td style="width: 250;" align="center">Ketua Kelompok</td>
						<td style="width: 200;" align="center">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 50;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">&nbsp;</td>
						<td style="width: 250;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 50;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">&nbsp;</td>
						<td style="width: 250;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 50;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">&nbsp;</td>
						<td style="width: 250;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 50;" align="center">&nbsp;</td>
						<td style="width: 200;" align="center"><b><u>KUSNADI</u></b></td>
						<td style="width: 250;" align="center"><b><u>ROENAH</u></b></td>
						<td style="width: 200;" align="center"><b><u>NUNUNG NURJANNAH</u></b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>