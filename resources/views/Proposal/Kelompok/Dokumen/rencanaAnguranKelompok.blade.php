<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cetak Rencana Angsuran Kelompok</title>
</head>
<body>
	<table style="font-family: arial;">
		<tr>
			<td>
				<table>
					<tr>
						<td width="450" align="left">KELOMPOK ANGGREK</td>
						<td width="450" align="right">Lampiran 3</td>
					</tr>
					<tr>
						<td width="450" align="left" style="font-size: 13px;">Alamat : DESA GARAWASTU</td>
						<td width="450" align="right" style="font-size: 13px;">Dokumen Proposal Kredit</td>
					</tr>
				</Table><Table>
					<tr>
						<td style="font-size: 2px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="font-size: 25px;"><b>RENCANA PENGEMBALIAN PINJAMAN</b></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="width: 200px;">ID Kelompok</td>
						<td style="width: 350px;">: &nbsp;201272312873872</td>
						<td style="width: 200px;">Permohonan</td>
						<td style="width: 300px;">: &nbsp;Rp. 33,000,000</td>
					</tr>
					<tr>
						<td style="width: 200px;">Nama Kelompok</td>
						<td style="width: 350px;">: &nbsp;ANGGREK</td>
						<td style="width: 200px;">Jasa</td>
						<td style="width: 300px;">: &nbsp;20.00% per tahun</td>
					</tr>
					<tr>
						<td style="width: 200px;">Desa</td>
						<td style="width: 350px;">: &nbsp;GARAWASTU</td>
						<td style="width: 200px;">Jangka Waktu</td>
						<td style="width: 300px;">: &nbsp;12 Bulan</td>
					</tr>
					<tr>
						<td style="width: 200px;">Pemanfaat</td>
						<td style="width: 350px;">: &nbsp;9 Orang</td>
						<td style="width: 200px;">Periodisasi</td>
						<td style="width: 300px;">: &nbsp;Satu Kali Setiap Bulan</td>
					</tr>

				</table>
				<br>
				<table>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
				</table>
				<table border="2" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px;">
					<tr height="40px">
						<td style="width: 70px;" align="center"><b>Angsuran Ke-</b></td>
						<td style="width: 110px;" align="center"><b>Pokok</b></td>
						<td style="width: 110px;" align="center"><b>Bunga</b></td>
						<td style="width: 110px;" align="center"><b>Total</b></td>
						<td style="width: 150px;" align="center"><b>Kumulatif</b></td>
						<td style="width: 150px;" align="center"><b>Saldo Pinjaman</b></td>
					</tr>
					<tr height="30px">
						<td style="width: 70px; background-color: gray;" align="center">&nbsp; </td>
						<td style="width: 110px; background-color: gray;" align="right">&nbsp;</td>
						<td style="width: 110px; background-color: gray;" align="right">&nbsp;</td>
						<td style="width: 110px; background-color: gray;" align="right">&nbsp;</td>
						<td style="width: 150px; background-color: gray;" align="right"> &nbsp;</td>
						<td style="width: 150px;" align="right">39,600,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">1 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">2 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">3 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">4 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">5 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">6 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">7 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">8 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">9 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">10 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">11 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
					<tr height="30px">
						<td style="width: 70px;" align="center">12 </td>
						<td style="width: 110px;" align="right"> 2,750,000</td>
						<td style="width: 110px;" align="right">550,000</td>
						<td style="width: 110px;" align="right">3,300,000</td>
						<td style="width: 150px;" align="right"> 3,300,000</td>
						<td style="width: 150px;" align="right">36,300,000</td>
					</tr>
				</table>
				<table>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Garawastu, 11 Desember 2022</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Ketua Kelompok</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center"><b><u>ROENAH</u></b></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</body>
</html>