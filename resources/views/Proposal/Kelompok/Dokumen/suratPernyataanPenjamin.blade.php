<html>
<head>
<title>Cetak Surat Pernyataan Penjamin</title>
</head>
<body>
	<table style="font-family: arial;" width="700">
		<tr>
			<td>
				<table>
					<tr>
						<td width="450" align="left">KELOMPOK ANGGREK</td>
						<td width="450" align="right">Lampiran 6.b</td>
					</tr>
					<tr>
						<td width="450" align="left" style="font-size: 13px;">Alamat : DESA GARAWASTU</td>
						<td width="450" align="right" style="font-size: 13px;">Dokumen Proposal Kredit</td>
					</tr>
				</Table><Table>
					<tr>
						<td style="font-size: 2px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td >&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="font-size: 25px;"><b>SURAT PERNYATAAN PENJAMIN</b></td>
					</tr>
					<tr>
						<td >&nbsp;</td>
					</tr>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table width="700px" style="font-size: 13px;">
					<tr> 
						<td align="justify" >
						&nbsp;&nbsp;&nbsp;&nbsp; Dengan menandatangani Surat Pernyataan ini, maka secara sadar dan penuh rasa tanggungjawab 
							menyatakan dengan sebenar-benarnya mengetahui dan menyetujui nama tertera dibawah ini untuk mengajukan 
							pinjaman sejumlah yang tertera.</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td> &nbsp;&nbsp;&nbsp;&nbsp; Menjamin melancaran pengembalian pinjaman dimaksud, sampai dengan pinjaman dimaksud Lunas.</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
					</tr>
				</table>
				<table border="2" cellpadding="0" cellspacing="0" align="center" style="font-size: 12px;">
					<tr height="40px">
						<td style="width: 30px;" align="center"><b>No</b></td>
						<td style="width: 170px;" align="center"><b>Nama Peminjam</b></td>
						<td style="width: 160px;" align="center"><b>Pengajuan Pinjaman</b></td>
						<td style="width: 140px;" align="center"><b>Nama Penjamin</b></td>
						<td style="width: 130px;" align="center"><b>Hubungan Keluarga</b></td>
						<td style="width: 70px;" align="center"><b>Tanda Tangan</b></td>
					</tr>
					<tr height="30px">
						<td style="width: 30px;" align="center">1 </td>
						<td style="width: 170px;" align="left"><b>ROENAH</b><br>NIK : 3210084910710001</td>
						<td style="width: 160px;" align="right">2,000,000</td>
						<td style="width: 140px;" align="left">KUSNADI</td>
						<td style="width: 130px;" align="center">Suami</td>
						<td style="width: 70px;" align="left">1</td>
					</tr>
				</table>
				<table border="1" cellpadding="0" cellspacing="0" align="left" style="font-size: 12px;">
				<tr height="30px">
						<td style="width: 30px; border-top: none; border-right: none;" align="center"></td>
						<td style="width: 169.5px; border-top: none; border-left: none;" align="left">JUMLAH</td>
						<td style="width: 155.5px; border-top: none;" align="right">2,000,000</td>
					</tr>
				</table>
				<br>
				<br>
				<br>
				<table width="700" style="font-size: 12px;">
					<tr>
						<td> &nbsp;&nbsp;&nbsp;&nbsp; Demikian pernyataan ini dibuat serta ditandatangani dihadapan Pengurus Kelompok dan Pemerintah Desa, untuk digunakan sebagaimana mestinya.</td>
					</tr>
				</table>
				<br>
				<br>
				<table>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">Garawastu, 19 Nopember 2018</td>
				</tr>
				<tr>
					<td width="350" align="center">Mengetahui,</td>
					<td width="350" align="center">Ketua Kelompok</td>
				</tr>
				<tr>
					<td width="350" align="center">Kepala Desa</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center">&nbsp;</td>
					<td width="350" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="350" align="center"><b><u>DEDI SETIADI</u></b></td>
					<td width="350" align="center"><b><u>ROENAH</u></b></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</body>
</html>