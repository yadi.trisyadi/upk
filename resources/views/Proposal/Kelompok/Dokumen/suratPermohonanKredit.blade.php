<html>
<head>
<title>Cetak Surat Permohonan Kredit</title>
</head>
<body>
<table border="0" style="width: 700px">
                <tr>
                    <td align="center" style="font-size: 22px;">KELOMPOK SIMPAN PINJAM KHUSUS PEREMPUAN</td>
                </tr>
                <tr>
                    <td align="center" style="font-weight: bold; font-size: 22px;"><b>ANGGREK</b></td>
                </tr>
                <tr>
                    <td align="center" style="font-size: 15px;"><i>Alamat: DESA GARAWASTU</i></td>
                </tr>
            </table>
<table style="font-family: arial; font-size: 14px; width:700px">
	<tr>
		<td>
			<table>
				<tr>
					<td style="border-bottom: 5px solid #000; width: 700px;" align="center"></td>
				</tr>
				<tr>
					<td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table style="font-style: 13px;">
				<tr>
					<td style="width: 140;">Nomor</td>
					<td style="width: 10;">:</td>
					<td style="width: 550;">  /ANGGREK/2022</td>
				</tr>
				<tr>
					<td style="width: 140;">Lampiran</td>
					<td style="width: 10;">:</td>
					<td style="width: 550;">1 (Satu) Berkas</td>
				</tr>
				<tr>
					<td style="width: 140;">Perihal</td>
					<td style="width: 10;">:</td>
					<td style="width: 550;"><b>Permohonan Kredit</b></td>
				</tr>

			</table>
			<table>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Kepada Yth :</td>
				</tr>
				<tr>
					<td>Ketua UPK Kecamatan Sindang</td>
				</tr>
				<tr>
					<td>Kepada Yth :</td>
				</tr>
				<tr>
					<td>Di Tempat</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Dengan Hormat,</td>
				</tr>
				<tr>
					<td>Yang bertandatangan dibawah ini :</td>
				</tr>
			</table>
			<table>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 100;">Nama Lengkap</td>
					<td style="width: 10;">:</td>
					<td >ROENAH</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 100;">Jabatan</td>
					<td style="width: 10;">:</td>
					<td >Ketua Kel. Anggrek</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 100;">Alamat</td>
					<td style="width: 10;">:</td>
					<td >Desa Garawastu</td>
				</tr>
			</table>
			<table style="width: 700px">
				<tr>
					<td style="font-size: 2;">&nbsp;</td>
				</tr>
				<tr>
					<td align="justify">Dalam hal ini bertindak atas nama diri sendiri dan anggota Kelompok ANGGREK, Dengan ini bermaksud mengajukan Kredit Sebesar Rp. 33,000,000 (Tiga Puluh Tiga Juta) untuk memenuhi kebutuhan tambahan modal usaha bagi 9 orang anggota. Kredit atau pinjaman tersebut diatas, akan kami kembalikan dalam jangka waktu 12 Bulan, ditambah jasa kredit, dengan sistem angsuran Bulanan (Satu Kali Setiap Bulan).
					</td>
				</tr>
				<tr>
					<td style="font-size: 2;">&nbsp;</td>
				</tr>
				<tr>
					<td>Sebagai bahan pertimbangan, bersama ini kami lampirkan :</td>
				</tr>
				<tr>
					<td style="font-size: 2;">&nbsp;</td>
				</tr>
			</table>
			<table>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 10;">1.</td>
					<td >Daftar Anggota</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 10;">2.</td>
					<td >Daftar Anggota Pemanfaat Pinjaman</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 10;">3.</td>
					<td >Rencana Pengembalian Pinjaman</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 10;">4.</td>
					<td >Surat Pernyataan Tanggung Renteng</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 10;">5.</td>
					<td >Surat Pernyataan Penjamin</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 10;">6.</td>
					<td >Fotocopy KTP dan KK Anggota Pemanfaat</td>
				</tr>
				<tr>
					<td style="width: 30;"></td>
					<td style="width: 10;">7.</td>
					<td >Lain-Lain</td>
				</tr>
			</table>
			<table>
				<tr>
					<td style="font-size: 2;">&nbsp;</td>
				</tr>
				<tr>
					<td>Demikian Permohonan Kami, atas perhatiannya kami ucapkan terima kasih.</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Garawastu, 11 Desember 2022</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">Ketua Kelompok</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td width="400">&nbsp;</td>
					<td align="center"><b><u>ROENAH</u></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
