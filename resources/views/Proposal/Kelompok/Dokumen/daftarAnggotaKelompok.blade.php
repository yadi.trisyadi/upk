<style>
    .tableAnggota {
        border: #000 1px solid;
        border-collapse: collapse;
        table-layout: inherit
    }

    body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        line-height: normal;
        }

</style>
<html>

<body>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>Kelompok</td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">Alamat : Jalan Jogja Kecil No. 16 Sindang Majalengka 45471</td>
                    </tr>
                    <tr>
                        <td style="font-size: 2px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
                    </tr>
                    {{-- <tr>
                        <td>&nbsp;</td>
                    </tr> --}}
                    <tr>

                        <td align="center" style="font-size: 18px;"><p><b>DAFTAR ANGGOTA KELOMPOK</b></p></td>
                    </tr>
                    {{-- <tr>
                        <td>&nbsp;</td>
                    </tr> --}}
                    <tr>
                        <td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <table>
                    @foreach ($dataKelompok as $dataKelompok)

                    <tr>
                        <td style="width: 150px;">ID Kelompok</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->kode_registrasi }}</td>
                        <td style="width: 150px;">Jenis Kelompok</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getTipeAnggota($dataKelompok->jenis_kelompok) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Nama Kelompok</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->nama_kelompok }}</td>
                        <td style="width: 150px;">Jenis Usaha</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getNameJenisUsaha($dataKelompok->jenis_usaha) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Desa</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ namaDesaKelompok($dataKelompok->desa) }}</td>
                        <td style="width: 150px;">Jenis Kegiatan</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getNameJenisKegiatan($dataKelompok->jenis_kegiatan) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Anggota</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->nama_ketua }}</td>
                        <td style="width: 150px;">Tingkat</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getTingkat($dataKelompok->tingkat) }}</td>
                    </tr>

                    @endforeach
                </table>
                <br>

                <table class="tableAnggota" border="1">



                    <tr>
                        <td style="width: 20px;" align="center"><b>No.</b></td>

                        <td style="width: 150px;" align="center"><b>Nama Lengkap</b></td>
                        <td style="width: 100px;" align="center"><b>Jabatan</b></td>
                        <td style="width: 50px;" align="center"><b>L/P</b></td>
                        <td style="width: 50px;" align="center"><b>Umur</b></td>
                        <td style="width: 150px;" align="center"><b>Jenis Usaha</b></td>
                        <td style="width: 150px;" align="center"><b>Status</b></td>
                    </tr>
                        @php
                            $i = 1;
                        @endphp
                    @foreach ($dataAnggota as $dataAnggota)
                    <tr>
                        <td style="width: 20px;">&nbsp; {{ $i++ }}</td>
                        <td style="width: 150px;"> &nbsp;{{ $dataAnggota->nama_lengkap }}</td>
                        <td style="width: 100px;">&nbsp; {{ getJabatan($dataAnggota->jabatan) }} </td>
                        <td style="width: 50px;" align="center">&nbsp;{{ $dataAnggota->jenis_kelamin }}</td>
                        <td style="width: 50px;" align="center"></td>
                        <td style="width: 150px;">&nbsp;{{ getNameJenisUsaha($dataAnggota->jenis_usaha) }}</td>
                        <td style="width: 150px;" align="center">-</td>
                    </tr>
                    @endforeach
                </table>
                <br>


                <table border="0" style="width: 720px">

                    <tr>
                        <td align="center" style="font-weight: bold">Ketua <br><br><br><br><u>Yayan Bambang</u></span></td>
                        <td align="center" style="font-weight: bold">Anggota <br><br><br><br>asdasd</td>
                        <td align="center" style="font-weight: bold">Anggota <br><br><br><br>LIA F LESTARI</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
