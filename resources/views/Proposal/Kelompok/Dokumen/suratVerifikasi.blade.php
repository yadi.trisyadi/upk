<style>
    .tableAnggota {
        border: #000 1px solid;
        border-collapse: collapse;
        table-layout: inherit
    }

    body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        line-height: normal;
        }

</style>
<html>

<body>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>TIM VERIFIKASI KECAMATAN SINDANG</td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">Alamat : Jalan Jogja Kecil No. 16 Sindang Majalengka 45471</td>
                    </tr>
                    <tr>
                        <td style="font-size: 2px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 25px;"><b>REKOMENDASI TIM VERIFIKASI</b></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <table>
                    @foreach ($dataKelompok as $dataKelompok)

                    <tr>
                        <td style="width: 150px;">Kode Registrasi</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->kode_registrasi }}</td>
                        <td style="width: 150px;">Jenis Kelompok</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getTipeAnggota($dataKelompok->jenis_kelompok) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Nama Kelompok</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->nama_kelompok }}</td>
                        <td style="width: 150px;">Jenis Usaha</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getNameJenisUsaha($dataKelompok->jenis_usaha) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Desa</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ namaDesaKelompok($dataKelompok->desa) }}</td>
                        <td style="width: 150px;">Jenis Kegiatan</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getNameJenisKegiatan($dataKelompok->jenis_kegiatan) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Nama Ketua</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->nama_ketua }}</td>
                        <td style="width: 150px;">Tingkat</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getTingkat($dataKelompok->tingkat) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">Alamat</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->alamat_kelompok }}</td>
                        <td style="width: 150px;">Fungsi</td>
                        <td style="width: 200px;">: &nbsp;&nbsp;{{ getFungsi($dataKelompok->fungsi) }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">No. Telp</td>
                        <td style="width: 250px;">: &nbsp;&nbsp;{{ $dataKelompok->telp_kelompok }}</td>
                        <td style="width: 150px;"></td>
                        <td style="width: 200px;">&nbsp;&nbsp;</td>
                    </tr>
                    @endforeach
                </table>
                <br>
                <table>
                    <tr>
                        <td style="border-top: 2px solid #000; width: 700px;" align="center"></td>
                    </tr>
                </table>
                <br>

                <table class="tableAnggota" border="1">



                    <tr>
                        <td style="width: 20px;" align="center"><b>No.</b></td>
                        <td 1><b>Nama Anggota</b></td>
                        <td style="width: 150px;" align="center"><b>Proposal</b></td>
                        <td style="width: 150px;" align="center"><b>Rekomendasi</b></td>
                        <td style="width: 150px;" align="center"><b>Catatan</b></td>
                    </tr>
                        @php
                            $i = 1;
                        @endphp
                    @foreach ($dataAnggota as $dataAnggota)
                    <tr>
                        <td style="width: 20px;" align="center">{{ $i++ }}</td>
                        <td style="width: 200px;" align="left"> &nbsp;{{ $dataAnggota->nama_lengkap }}</td>
                        <td style="width: 150px;" align="right">{{ number_format($dataAnggota->jumlah_pengajuan) }}&nbsp; </td>
                        <td style="width: 150px;" align="center">&nbsp;{{ number_format($dataAnggota->jumlah_rekomendasi) }}</td>
                        <td style="width: 150px;" align="center"></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="2"><center>Jumlah</center></td>

                        <td style="width: 150px;" align="right">{{ number_format($jumlahTotal) }}&nbsp; </td>
                        <td style="width: 150px;" align="center"></td>
                        <td style="width: 150px;" align="center"></td>
                    </tr>
                </table>

                <table style="width: 700px">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>Kesimpulan :</b></td>
                    </tr>
                    <tr>
                        <td>Berdasarkan hasil kunjungan lapangan dan wawancara, Tim Verifikasi Kecamatan Sindang
                            menyimpulkan bahwa kelompok dengan identitas tersebut diatas dinyatakan :</td>
                    </tr>
                    <tr>
                        <td style="font-size: 2px;">&nbsp;</td>
                    </tr>
                </table>
                <table style="font-size: 12px; ">
                    <tr>
                        <td style="width: 30; border-style: none;">&nbsp;</td>
                        <td style="width: 20; border-style: solid;">&nbsp;</td>
                        <td style="width: 50;"> <b>&nbsp;&nbsp;&nbsp;LAYAK</b></td>
                        <td style="width: 30; border-style: none;">&nbsp;</td>
                        <td style="width: 20; border-style: solid;">&nbsp;</td>
                        <td style="width: 100;"> <b>&nbsp;&nbsp;&nbsp;TIDAK LAYAK</b></td>
                    </tr>
                </table>
                <table style="width: 700px">
                    <tr>
                        <td style="font-size: 2px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Untuk memperoleh pinjaman berdasarkan rekomendasi dengan uraian sebagai berikut:</td>
                    </tr>
                    <tr>
                        <td style="font-size: 2px;">&nbsp;</td>
                    </tr>
                </table>
                <table class="tableAnggota" border="1" style="width: 700px">
                    <tr>
                        <td style="width: 120px;" align="center"><b>Deskripsi</b></td>
                        <td style="width: 100px;" align="center"><b>Tanggal</b></td>
                        <td style="width: 150px;" align="center"><b>Alokasi</b></td>
                        <td style="width: 100px;" align="center"><b>Jasa</b></td>
                        <td style="width: 100px;" align="center"><b>Jangka</b></td>
                        <td style="width: 100px;" align="center"><b>Sistem</b></td>
                    </tr>
                    <tr>
                        <td style="width: 120px;" ><b>Proposal</b></td>
                        <td style="width: 100px;" align="center">{{ $dataKelompok->tanggal_proposal }}</td>
                        <td style="width: 150px;" align="center">{{ number_format($dataKelompok->jumlah_pengajuan) }}</td>
                        <td style="width: 100px;" align="center">{{ $dataKelompok->jasa }}</td>
                        <td style="width: 100px;" align="center">{{ $dataKelompok->jangka_waktu }}</td>
                        <td style="width: 100px;" align="center">Sistem</td>
                    </tr>
                    <tr>
                        <td style="width: 100px;"><b>Rekomendasi</b></td>
                        <td style="width: 100px;" align="center">Tanggal</td>
                        <td style="width: 150px;" align="center">Alokasi</td>
                        <td style="width: 100px;" align="center">Jasa</td>
                        <td style="width: 100px;" align="center">Jasa</td>
                        <td style="width: 100px;" align="center">Sistem</td>
                    </tr>


                </table>
                <table style="width: 700px">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Atas hasil rekomendasi tersebut di atas, selanjutnya dibahas pada Rapat Tim Pendanaan atau
                            Musyawarah Pendanaan Perguliran di Kecamatan Sindang sesuai dengan ketentuan dan aturan yang
                            berlaku.</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <style>
                    .anggota{
                        text-decoration: underline;
                        margin-top: 500px
                    }
                </style>
                <table border="0" style="width: 720px">
                    <tr>
                        <td colspan="3" align="center">Tim Verifikasi Kecamatan<br><br></td>
                    </tr>
                    <tr>
                        <td align="center" style="font-weight: bold">Ketua <br><br><br><br><u>Yayan Bambang</u></span></td>
                        <td align="center" style="font-weight: bold">Anggota <br><br><br><br>asdasd</td>
                        <td align="center" style="font-weight: bold">Anggota <br><br><br><br>LIA F LESTARI</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
