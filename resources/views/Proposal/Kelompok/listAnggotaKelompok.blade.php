@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
    type="text/css">
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Proposal @endslot
@slot('title') Proposal Kelompok @endslot
@endcomponent
@foreach ($data as $data )

<div class="row">
    <div class="col-xl-4">
        <div class="card overflow-hidden">
            <div class="bg-primary bg-soft">
                <div class="row">
                    <div class="col-7">
                        <div class="text-primary p-3">
                            <h5 class="text-primary">{{ $data->nama_kelompok }}</h5>
                            <p>{{ $data->alamat_kelompok }},<br>{{ $data->telp_kelompok }}<br>
                                {{ namaDesaKelompok($data->desa) }} </p>
                        </div>
                    </div>
                    <div class="col-5 align-self-end">
                        <img src="{{ URL::asset('/assets/images/profile-img.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">
                    {{--  <div class="col-sm-4">
                            <div class="avatar-md profile-user-wid mb-4">
                                <img src="{{ isset(Auth::user()->avatar) ? asset(Auth::user()->avatar) : asset('/assets/images/users/avatar-1.jpg') }}"
                    alt="" class="img-thumbnail rounded-circle">
                </div>
                <h5 class="font-size-15 text-truncate">{{ Auth::user()->name }}</h5>
                <p class="text-muted mb-0 text-truncate">UI/UX Designer</p>
            </div> --}}


        </div>
    </div>
</div>
<!-- end card -->


</div>

<div class="col-xl-8">

    <div class="row">
        <div class="col-md-4">
            <div class="card mini-stats-wid">
                <div class="card-body">
                    <div class="media">
                        <div class="media-body">
                            <p class="text-muted fw-medium mb-2">Jumlah Anggota</p>
                            <h5 class="mb-0">{{ $jumlahAnggota }}</h5>
                        </div>

                        <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                            <span class="avatar-title">
                                <i class="fas fa-user fa-2x"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mini-stats-wid">
                <div class="card-body">
                    <div class="media">
                        <div class="media-body">
                            <p class="text-muted fw-medium mb-2">Pemanfaat</p>
                            <h5 class="mb-0">{{ $jumlahAnggota }}</h5>
                        </div>

                        <div class="avatar-sm align-self-center mini-stat-icon rounded-circle bg-primary">
                            <span class="avatar-title">
                                <i class="fas fa-hand-holding-usd fa-2x"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mini-stats-wid">
                <div class="card-body">
                    <div class="media">
                        <div class="media-body">
                            <p class="text-muted fw-medium mb-2">Alokasi Pinjaman</p>
                            <h5 class="mb-0" id="totalProposal">{{ number_format($alokasiPinjaman) }}</h5>
                        </div>

                        <div class="avatar-sm align-self-center mini-stat-icon rounded-circle bg-primary">
                            <span class="avatar-title">
                                <i class="fas fa-wallet fa-2x"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<form method="POST" action="{{ url('add-proposal') }}">
    @csrf
    <input type="hidden" name='grandTotalProposal' id="grandTotalProposal" value="{{ $alokasiPinjaman }}">
    <input type="hidden" name="idKelompok" value="{{ $data->id }}">
    <div class="row">
        <div class="card">
            <div class="card-body">
                <p>
                    <a class="btn btn-primary btn-sm" href="{{ url('tambah-anggota-proposal',$data->id) }}">Tambah
                        Anggota</a>
                </p>
                <h4 class="card-title mb-4">Anggota Kelompok</h4>
                <div class="table-responsive">
                    <table class="table table-nowrap table-hover mb-0 datatable" id="proposalAnggota">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIK</th>
                                <th scope="col">Nama Lengkap</th>
                                <th scope="col">Jabatan</th>
                                <th scope="col">Jenis Usaha</th>
                                <th scope="col">Proposal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i = 1;
                            ?>
                            @foreach ($dataAnggota as $dataAnggota)

                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $dataAnggota->nik }}</td>
                                <td>{{ $dataAnggota->nama_lengkap }}</td>
                                <td>{{ getJabatan($dataAnggota->jabatan) }}</td>
                                <td>{{ getNameJenisUsaha($dataAnggota->jenis_usaha) }}</td>
                                <td><input type="text" name="proposal[]"
                                        class="form-control form-control-sm jumlahProposal"
                                        value="{{ $dataAnggota->pengajuan_proposal }}" size='4'>
                                    <input type="hidden" name="idAnggota[]" value="{{ $dataAnggota->id }}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- <a class="btn btn-primary" href="{{ url('proposal-kelompok/create') }}">Kembali</a> --}}
            </div>
        </div>
        <div class="row">
            <div class="d-flex flex-wrap gap-2">
                <a class="btn btn-secondary waves-effect pull-right"
                    href="{{ url('proposal-kelompok/create') }}">Kembali
                </a>
                <button type="submit" class="btn btn-primary waves-effect waves-light">
                    Submit
                </button>

            </div>
        </div>
    </div>
</form>
@endforeach

<!-- end row -->



@endsection
@section('script')
<!-- apexcharts -->
<script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>

<script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
<!-- profile init -->
<script src="{{ URL::asset('/assets/js/pages/profile.init.js') }}"></script>

<script>
    $("#proposalAnggota").on('input', '.jumlahProposal', function () {
        var calculated_total_sum = 0;

        $("#proposalAnggota .jumlahProposal").each(function () {
            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                calculated_total_sum += parseFloat(get_textbox_value);
            }
        });
        var totalProposal = parseInt(calculated_total_sum).toLocaleString();
        $("#totalProposal").html(totalProposal);
        $('#grandTotalProposal').val(calculated_total_sum);
    });

</script>

@endsection
