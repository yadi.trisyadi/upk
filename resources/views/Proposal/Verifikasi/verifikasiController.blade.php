<?php

namespace App\Http\Controllers\Proposal\Kelompok;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class VerifikasiProposalController extends Controller
{
    public function verifikasiProposal($id){
        $data = DB::table('tb_proposal_kelompok')->where('id',$id)->get();
        $dataKelompok = DB::table('tb_kelompok')->where('id',$data[0]->id_kelompok)->get();
        $jumlahAnggota = DB::table('tb_anggota_kelompok')->where('id_kelompok',$data[0]->id_kelompok)->count();
        $jumlahPemanfaat = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 0
            ])->count();



        $alokasiPinjaman = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 0
            ])->sum('jumlah_pengajuan');
        // $dataAnggota = DB::select("SELECT a.id,a.nik,a.nama_lengkap,a.jabatan,a.jenis_usaha,b.jumlah_pengajuan FROM tb_anggota_kelompok a, tb_proposal_detail b where a.id = b.id_anggota and b.status='0' and b.id_proposal='$id'");


        $dataAnggota = DB::select("SELECT a.id,a.nik,a.nama_lengkap,a.jabatan,a.jenis_usaha,b.jumlah_pengajuan FROM tb_anggota_kelompok a, tb_proposal_detail b where a.id = b.id_anggota and b.status='0' and b.id_proposal='$id'");
        return view('Proposal.Verifikasi.verifikasiShow',[
            'idKelompok' => $data[0]->id_kelompok,
            'idProposal' => $data[0]->id,
            'dataKelompok' => $dataKelompok,
            'jumlahAnggota' => $jumlahAnggota,
            'jumlahPemanfaat' => $jumlahPemanfaat,
            'alokasiPinjaman' => $alokasiPinjaman,
            'dataAnggota' => $dataAnggota
        ])->with([
            'title' => 'Verifikasi Proposal'
        ]);
    }

    public function store(Request $request){
       $idKelompok = $request->idKelompok;
       $idProposal = $request->idProposal;
       $dataPropposal = DB::table('tb_proposal_kelompok')->where('id',$idProposal);
       $alokasiPinjaman = $request->grandTotalProposal;

       $sum = 0;
        foreach($request->rekomendasi as $key => $value){
            $idAnggota = $request->idAnggota[$key];
            $jumlahProposal = $value;
            // echo "$idAnggota - $jumlahProposal<br>";
            if($jumlahProposal > 0){
                $result[] = array('idAnggota' => $idAnggota, 'jumlahProposal'=>$jumlahProposal);
            }

            $sum += $jumlahProposal;
        }

        $count = count($result);

        return redirect('Proposal.Verifikasi.verifikasiCreate',[
            'dataProposal' => $dataPropposal,
            'idKelompok' => $idKelompok,
            'alokasiPinjaman' => $alokasiPinjaman,
            'anggota' => $result


        ]);
    }
}
