@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
    type="text/css">
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Verifikasi @endslot
@slot('title') Verifikasi Proposal Kelompok @endslot
@endcomponent

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                Nama Kelompok
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-success">Proposal Berhasil Di Verifikasi</div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link mb-2 active" id="v-pills-home-tab" data-bs-toggle="pill" href="#v-pills-home" role="tab"
                aria-controls="v-pills-home" aria-selected="true">Data Verifikasi</a>
            <a class="nav-link mb-2" id="v-pills-profile-tab" data-bs-toggle="pill" href="#v-pills-profile" role="tab"
                aria-controls="v-pills-profile" aria-selected="false">Pemanfaat</a>
            <a class="nav-link mb-2" href="#" role="tab"
                aria-controls="v-pills-messages" aria-selected="false">Print Lembar Hasil Verifikasi</a>

        </div>
    </div>
    <div class="col-md-10">
        <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Proposal</h5>
                                </div>
                                @foreach ($dataProposal as $dataProposal)

                                <div class="card-body">
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Tanggal
                                            Registrasi</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm" id="tanggalRegistrasi"
                                                name="tanggalRegistrasi" value="{{ $dataProposal->tanggal_proposal }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Kode
                                            Registrasi</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm" id="kodeRegistrasi"
                                                name="kodeRegistrasi" value="{{ $dataProposal->kode_registrasi }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Proposal</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm" id="pengajuan" name="pengajuan"
                                                value="{{ number_format($dataProposal->jumlah_pengajuan) }}" readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Pemanfaat</label>
                                        <div class="col-sm-7">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" id="pemanfaat" name="pemanfaat"
                                                    value="{{ number_format($dataProposal->jumlah_pemanfaat) }}" readonly>
                                                <div class="input-group-text"> Orang</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Jasa</label>
                                        <div class="col-sm-7">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" id="jasa" name="jasa"
                                                    value="{{ $dataProposal->jasa }}" readonly>
                                                <div class="input-group-text"> % per Tahun</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Jangka</label>
                                        <div class="col-sm-7">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" id="jangkaWaktu"
                                                    name="jangkaWaktu" value="{{ $dataProposal->jangka_waktu }}"
                                                    readonly>
                                                <div class="input-group-text">Bulan</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Periodisasi</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm" id="sistemAngsuran"
                                                name="sistemAngsuran"
                                                value="{{ getSistemAngsuran($dataProposal->sistem_angsuran) }}"
                                                readonly>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Hasil Verifikasi</h5>
                                </div>
                                <div class="card-body">
                                    @foreach ($dataVerifikasi as $dataVerifikasi)

                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Tanggal
                                            Verifikasi</label>
                                        <div class="col-sm-7">
                                            <input class="form-control form-control-sm" type="text" name="tanggalVerifikasi"
                                                id="tanggalVerifikasi" value="{{ $dataVerifikasi->tanggal_verifikasi }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Kode
                                            Registrasi</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm" id="kodeRegistrasiVerifikasi"
                                                name="kodeRegistrasiVerifikasi"
                                                value="{{ $dataVerifikasi->kode_registrasi }}" readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Rekomendasi</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm" id="pinjaman" name="pinjaman"
                                                value="{{ number_format($dataVerifikasi->jumlah_pengajuan) }}" readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Pemanfaat</label>
                                        <div class="col-sm-7">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" id="pemanfaatVerifikasi"
                                                    name="pemanfaatVerifikasi"
                                                    value="{{ $dataVerifikasi->jumlah_pemanfaat }}" readonly>
                                                <div class="input-group-text"> Orang</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Jasa</label>
                                        <div class="col-sm-7">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" id="jasaVerifikasi"
                                                    name="jasaVerifikasi" value="{{ $dataVerifikasi->jasa }}" readonly>
                                                <div class="input-group-text"> % per Tahun</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Jangka</label>
                                        <div class="col-sm-7">
                                            <div class="input-group input-group-sm">
                                                <input type="text" class="form-control" id="jangkaWaktuVerifikasi"
                                                    name="jangkaWaktuVerifikasi"
                                                    value="{{ $dataVerifikasi->jangka_waktu }}" readonly>
                                                <div class="input-group-text">Bulan</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input" class="col-sm-5 col-form-label">Periodisasi
                                            </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm"
                                                value="{{ getSistemAngsuran($dataVerifikasi->sistem_angsuran) }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Keterangan</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control form-control-sm"
                                                value="{{ getKeteranganVerifikasi($dataVerifikasi->keterangan) }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <label for="horizontal-firstname-input"
                                            class="col-sm-5 col-form-label">Memo</label>
                                        <div class="col-sm-7">
                                            <textarea class="form-control form-control-sm" id="memo" name="memo"
                                                disabled>{{ $dataVerifikasi->memo }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </p>

            </div>
            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <p>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>NIK</th>
                                <th>Nama Lengkap</th>
                                <th>Jenis Kelamin</th>
                                <th>Proposal</th>
                                <th>Rekomendasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dataAnggota as $dataAnggota)
                            <tr>
                                <td>{{ $dataAnggota->nik }}</td>
                                <td>{{ $dataAnggota->nama_lengkap }}</td>
                                <td>{{ $dataAnggota->jenis_kelamin }}</td>
                                <td>{{ number_format($dataAnggota->jumlah_pengajuan) }}</td>
                                <td>{{ number_format($dataAnggota->jumlah_rekomendasi) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </p>

            </div>
            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <p>PRINT</p>


            </div>
        </div>
    </div>






    <!-- end row -->



    @endsection
    @section('script')
    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>

    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
    <!-- profile init -->
    {{-- <script src="{{ URL::asset('/assets/js/pages/profile.init.js') }}"></script> --}}

    <script>
        $('#tanggalVerifikasi').datepicker({
            autoclose: 1,
            todayHighlight: 1,
            forceParse: 0,
            format: 'yyyy-mm-dd'

        });

        $("#proposalAnggota").on('input', '.rekomendasi', function () {
            var calculated_total_sum = 0;

            $("#proposalAnggota .rekomendasi").each(function () {
                var get_textbox_value = $(this).val();
                if ($.isNumeric(get_textbox_value)) {
                    calculated_total_sum += parseFloat(get_textbox_value);
                }
            });
            var totalProposal = parseInt(calculated_total_sum).toLocaleString();
            $("#totalProposal").html(totalProposal);
            $('#grandTotalProposal').val(calculated_total_sum);
        });

    </script>

    @endsection
