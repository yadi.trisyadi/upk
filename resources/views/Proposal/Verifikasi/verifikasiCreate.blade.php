@extends('layouts.master')

@section('title'){{$title}} @endsection

@section('css')
<link href="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet"
    type="text/css">
<link href="{{ URL::asset('/assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

@component('components.breadcrumb')
@slot('li_1') Verifikasi @endslot
@slot('title') Verifikasi Proposal Kelompok @endslot
@endcomponent

<div class="row">
    <div class="card">
        <div class="card-body">
            Nama Kelompok
        </div>
    </div>

</div>
{{-- <form method="POST" action="{{ route('verifikasi-proposal.create') }}"> --}}
    <form action="{{ url('verifikasi-proposal-store') }}"  method="post" class="custom-validation">
        @csrf
    {{-- @csrf --}}
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h5>Proposal</h5>
            </div>
            @foreach ($dataProposal as $dataProposal)

            <div class="card-body">
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Tanggal Registrasi</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="tanggalRegistrasi" name="tanggalRegistrasi" value="{{ $dataProposal->tanggal_proposal }}" readonly>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Kode Registrasi</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="kodeRegistrasi" name="kodeRegistrasi" value="{{ $dataProposal->kode_registrasi }}" readonly>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Alokasi Pengajuan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="pengajuan" name="pengajuan" value="{{ $dataProposal->jumlah_pengajuan }}" readonly>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Pemanfaat</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" class="form-control" id="pemanfaat" name="pemanfaat" value="{{ $dataProposal->jumlah_pemanfaat }}" readonly>
                            <div class="input-group-text"> Orang</div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Jasa</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" class="form-control" id="jasa" name="jasa" value="{{ $dataProposal->jasa }}" readonly>
                            <div class="input-group-text"> % per Tahun</div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Jangka</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" class="form-control" id="jangkaWaktu" name="jangkaWaktu" value="{{ $dataProposal->jangka_waktu }}" readonly>
                            <div class="input-group-text">Bulan</div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Sistem Angsuran</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="sistemAngsuran" name="sistemAngsuran" value="{{ getSistemAngsuran($dataProposal->sistem_angsuran) }}" readonly>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
   @foreach ($anggota as $anggota)
        <input type="hidden" name="idAnggota[]" value="{{ $anggota['idAnggota'] }}">
        <input type="hidden" name="jumlahProposal[]" value="{{ $anggota['jumlahProposal'] }}">
    @endforeach
    <input type="hidden" name="idKelompok" value="{{ $idKelompok }}">
    <input type="hidden" name="idProposal" value="{{ $dataProposal->id }}">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
               <h5>Hasil Verifikasi</h5>
            </div>
            <div class="card-body">
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Tanggal Verifikasi</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" name="tanggalVerifikasi" id="tanggalVerifikasi">
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Kode Registrasi</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="kodeRegistrasiVerifikasi" name="kodeRegistrasiVerifikasi" value="{{ $dataProposal->kode_registrasi }}" readonly>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Alokasi pinjaman</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="pinjaman" name="pinjaman" value="{{ $alokasiPinjaman }}">
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Pemanfaat</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" class="form-control" id="pemanfaatVerifikasi" name="pemanfaatVerifikasi" value="{{ $jumlahPemanfaat }}">
                            <div class="input-group-text"> Orang</div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Jasa</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" class="form-control" id="jasaVerifikasi" name="jasaVerifikasi" value="{{ $dataProposal->jasa }}">
                            <div class="input-group-text"> % per Tahun</div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Jangka</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" class="form-control" id="jangkaWaktuVerifikasi" name="jangkaWaktuVerifikasi" value="{{ $dataProposal->jangka_waktu }}">
                            <div class="input-group-text">Bulan</div>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Sistem Angsuran</label>
                    <div class="col-sm-8">
                        <select name="sistemAngsuranVerifikasi" class="form-control select2">
                            <option value="1">Bulanan</option>
                            <option value="2">2 Bulanan</option>
                            <option value="3">3 Bulanan</option>
                            <option value="4">4 Bulanan</option>
                            <option value="5">5 Bulanan</option>
                            <option value="6">6 Bulanan</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Keterangan</label>
                    <div class="col-sm-8">
                        <select name="keterangan" id="keterangan" class="form-control select2" required>
                            <option value="1">Layak</option>
                            <option value="0">Tidak Layak</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-2">
                    <label for="horizontal-firstname-input" class="col-sm-4 col-form-label">Memo</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" id="memo" name="memo"></textarea>
                    </div>
                </div>
                <div class="row mb-2">
                    <input type="submit" class="btn btn-primary" value="Verifikasi">
                </div>
            </div>
        </div>
    </div>
</div>
</form>





<!-- end row -->



@endsection
@section('script')
<!-- apexcharts -->
<script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ url('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ url('assets/js/pages/ecommerce-select2.init.js') }}"></script>

<script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('/assets/libs/datatables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/pages/datatables.init.js') }}"></script>
<!-- profile init -->
{{-- <script src="{{ URL::asset('/assets/js/pages/profile.init.js') }}"></script> --}}

<script>
    $('#tanggalVerifikasi').datepicker({
        autoclose: 1,
        todayHighlight: 1,
        forceParse: 0,
        format:'yyyy-mm-dd'

    });

    $("#proposalAnggota").on('input', '.rekomendasi', function () {
        var calculated_total_sum = 0;

        $("#proposalAnggota .rekomendasi").each(function () {
            var get_textbox_value = $(this).val();
            if ($.isNumeric(get_textbox_value)) {
                calculated_total_sum += parseFloat(get_textbox_value);
            }
        });
        var totalProposal = parseInt(calculated_total_sum).toLocaleString();
        $("#totalProposal").html(totalProposal);
        $('#grandTotalProposal').val(calculated_total_sum);
    });

</script>

@endsection
