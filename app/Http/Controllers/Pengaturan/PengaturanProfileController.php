<?php

namespace App\Http\Controllers\Pengaturan;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;




class PengaturanProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select('SELECT a.id,a.kode,a.nama_upk,a.alamat_upk,b.name as propinsi, c.name as kota, d.name as kecamatan, e.name as desa, a.telp_upk,a.email_upk,a.website_upk,a.ketua_upk,a.sekretaris_upk,a.bendahara_upk,a.ketua_bkad,a.ketua_bpupk,a.logo_upk from tb_profil a, indonesia_provinces b, indonesia_cities c, indonesia_districts d, indonesia_villages e where a.propinsi_upk = b.id and a.kota_kab_upk = c.id and a.kecamatan_upk = d.id and a.desa_upk = e.id');

        $count = DB::table('tb_profil')->count();
        return view('Pengaturan.Profile.profileShow',['data' => $data])->with([
            'title'=> 'Profile',
            'count' => $count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = DB::table('indonesia_provinces')->get();
        return view('Pengaturan.Profile.profileCreate',['provinces' => $provinces])->with([
            'title'=> 'Profile'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imageName = '';
        $input =$request->all();
        if ($image = $request->file('fileGambar')) {
            $destinationPath = 'images/logo-upk';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $imageName = $destinationPath.'/'.$profileImage;
            $image->move($destinationPath, $profileImage);
            $input['fileGambar'] = "$profileImage";
        }else{
            unset($input['fileGambar']);
        }

        DB::table('tb_profil')->insert([
            'kode' => $request->kodeUpk,
            'nama_upk' => $request->namaUpk,
            'telp_upk' => $request->telepon,
            'email_upk' => $request->email,
            'alamat_upk' => $request->alamat,
            'propinsi_upk' => $request->provinsi,
            'kota_kab_upk' => $request->kota,
            'kecamatan_upk' => $request->kecamatan,
            'desa_upk' => $request->desa,
            'website_upk' => $request->website,
            'ketua_upk' => $request->ketua,
            'sekretaris_upk' => $request->sekretaris,
            'bendahara_upk' => $request->bendahara,
            'ketua_bkad' => $request->ketuaBkad,
            'ketua_bpupk' => $request->ketuaBpupk,
            'logo_upk' => $imageName,
            'created_at' => Carbon::now()
        ]);

        return redirect('pengaturan-profile')->with('success', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return 'asdads';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('tb_profil')->where('id', $id)->get();
        $kota_kab_upk = $data[0]->kota_kab_upk;
        $desa_upk = $data[0]->desa_upk;
        $provinces = DB::table('indonesia_provinces')->get();
        $cities = DB::table('indonesia_cities')->get();
        $getCitiesCode = DB::select("SELECT `code` from indonesia_cities where id='$kota_kab_upk'");
        $kecamatan = DB::table('indonesia_districts')->where([
            'city_code' => $getCitiesCode[0]->code,
        ]) ->get();

        $getKecamatanCode = DB::select("SELECT `district_code` from indonesia_villages where id='$desa_upk'");
        $desa = DB::table('indonesia_villages')->where([
            'district_code' => $getKecamatanCode[0]->district_code
        ])->get();
        return view('Pengaturan.Profile.profileEdit',[
            'data' => $data,
            'provinces' => $provinces,
            'cities' => $cities,
            'kecamatan' => $kecamatan,
            'desa' => $desa
            ])->with([
            'title' => 'Profil'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldImage = DB::select('SELECT logo_upk from tb_profil where id='.$id.'');
        // if($oldImage[0]->logo_upk){
        //     return 'ada';
        // }else{
        //     return 'ga';
        // }
        $imageName = $oldImage[0]->logo_upk;
        $input =$request->all();
        if ($image = $request->file('fileGambar')) {
            $destinationPath = 'images/logo-upk';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $imageName = $destinationPath.'/'.$profileImage;
            unlink($oldImage[0]->logo_upk);
            $image->move($destinationPath, $profileImage);
            $input['fileGambar'] = "$profileImage";
        }else{
            unset($input['fileGambar']);
        }

        DB::table('tb_profil')->where('id',$id)->update([
            'kode' => $request->kodeUpk,
            'nama_upk' => $request->namaUpk,
            'telp_upk' => $request->telepon,
            'email_upk' => $request->email,
            'alamat_upk' => $request->alamat,
            'propinsi_upk' => $request->provinsi,
            'kota_kab_upk' => $request->kota,
            'kecamatan_upk' => $request->kecamatan,
            'desa_upk' => $request->desa,
            'website_upk' => $request->website,
            'ketua_upk' => $request->ketua,
            'sekretaris_upk' => $request->sekretaris,
            'bendahara_upk' => $request->bendahara,
            'ketua_bkad' => $request->ketuaBkad,
            'ketua_bpupk' => $request->ketuaBpupk,
            'logo_upk' => $imageName,
            'updated_at' => Carbon::now()
        ]);

        return redirect('pengaturan-profile')->with('success', 'Data Berhasil Di Update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
