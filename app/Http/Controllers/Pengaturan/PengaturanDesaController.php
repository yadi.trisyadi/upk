<?php

namespace App\Http\Controllers\Pengaturan;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PengaturanDesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = DB::table('tb_desa')->get();
        // $count = DB::table('tb_profil')->count();
        return view('Pengaturan.Desa.desaShow',['data' => $data])->with([
            'title'=> 'Desa',
            // 'count' => $count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $desaCode = DB::table('tb_profil')->get();
       $desa = getDesa($desaCode[0]->desa_upk);

        return view('Pengaturan.Desa.desaCreate',['desaCode' => $desa])->with([
            'title' => 'Desa'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $desaLainnya = $request->desaLainnya;
        if($desaLainnya){
            $codeDesa =  $string = rand(0,9999999);
            $namaDesa = $desaLainnya;
            $lainnya = 1;
        }else{
        $codeDesa = $request->desa;
        $getNameDesa = getNameDesa($codeDesa);
        $namaDesa = $getNameDesa[0]->name;
        $lainnya = 0;
        }
        $alamat = $request->alamat;
        $telp = $request->telp;
        $kepalaDesa = $request->kepalaDesa;
        $status = $request->status;

        $request->validate([
            'desa' => 'required|unique:tb_desa,kode'
        ]);

        DB::table('tb_desa')->insert([
            'kode' => $codeDesa,
            'nama_desa' => $namaDesa,
            'kepala_desa' => $kepalaDesa,
            'telp' => $telp,
            'alamat_desa' => $alamat,
            'status' => $status,
            'lainnya' => $lainnya,
            'created_at' => Carbon::now()
        ]);

        return redirect('pengaturan-desa')->with([
            'title' => 'Desa'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = DB::table('tb_profil')->get();
        $data = DB::table('tb_desa')->where('id',$id)->get();
        $desa = getDesa($profile[0]->desa_upk);
        return view('Pengaturan.Desa.desaEdit',['data' => $data,'desa'=>$desa])->with([
            'title' => 'Edit Desa'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        $desaLainnya = $request->desaLainnya;
        if($desaLainnya){
            $codeDesa =  $string = rand(0,9999999);
            $namaDesa = $desaLainnya;
            $lainnya = 1;
        }else{
        $codeDesa = $request->desa;
        $getNameDesa = getNameDesa($codeDesa);
        $namaDesa = $getNameDesa[0]->name;
        $lainnya = 0;
        }
        $alamat = $request->alamat;
        $telp = $request->telp;
        $kepalaDesa = $request->kepalaDesa;
        $status = $request->status;

        // $request->validate([
        //     'desa' => 'required|unique:tb_desa,kode'
        // ]);

        $cek = DB::table('tb_desa')->where('id',$id)->get();
        $cekLainnya = $cek[0]->lainnya;
        if($cekLainnya == 1){

            DB::table('tb_desa')->where('id',$id)->update([
                'nama_desa' => $namaDesa,
                'kepala_desa' => $kepalaDesa,
                'telp' => $telp,
                'alamat_desa' => $alamat,
                'status' => $status,
                'lainnya' => $lainnya,
                'created_at' => Carbon::now()
            ]);
        }else{
            DB::table('tb_desa')->where('id',$id)->update([
                'kode' => $codeDesa,
                'nama_desa' => $namaDesa,
                'kepala_desa' => $kepalaDesa,
                'telp' => $telp,
                'alamat_desa' => $alamat,
                'status' => $status,
                'lainnya' => $lainnya,
                'created_at' => Carbon::now()
            ]);
        }

        return redirect('pengaturan-desa')->with([
            'title' => 'Desa'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
