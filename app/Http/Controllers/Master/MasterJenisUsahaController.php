<?php

namespace App\Http\Controllers\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MasterJenisUsahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('m_jenis_usaha')->get();
        return view('Master.JenisUsaha.jenisUsahaShow', ['data' => $data])->with([
            'title' => 'Master Jenis Usaha'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Master.JenisUsaha.jenisUsahaCreate')->with([
            'title' => 'Create Jenis Usaha'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenisUsaha' => 'required|unique:m_jenis_usaha,jenis_usaha'
        ]);

        DB::table('m_jenis_usaha')->insert([
            'jenis_usaha' => $request->jenisUsaha,
            'keterangan' => $request->keterangan,
            'created_at' => Carbon::now()
        ]);

        return redirect('master-jenis-usaha')->with([
            'title' => 'Master Jenis Usaha',
            'success' => 'Data Berhasil Ditambah'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('m_jenis_usaha')->where('id',$id)->get();
        return view('Master.JenisUsaha.jenisUsahaEdit',['data' => $data])->with([
            'title' => 'Master Jenis Usaha'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('m_jenis_usaha')->where('id',$id)->update([
            'jenis_usaha' => $request->jenisUsaha,
            'keterangan' => $request->keterangan,
            'updated_at' => Carbon::now()
        ]);

        return redirect('master-jenis-usaha')->with([
            'title' => 'Master Jenis Usaha',
            'success' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('m_jenis_usaha')->where('id', $id)->delete();
        return redirect('master-jenis-usaha')->with([
            'title' => 'Master Jenis Usaha',
            'success' => 'Data Berhasil Dihapus'
        ]);
    }
}
