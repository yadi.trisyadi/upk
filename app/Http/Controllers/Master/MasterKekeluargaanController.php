<?php

namespace App\Http\Controllers\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MasterKekeluargaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('m_kekeluargaan')->get();
        return view('Master.Kekeluargaan.kekeluargaanShow', ['data' => $data])->with([
            'title' => 'Master Kekeluargaan'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Master.Kekeluargaan.kekeluargaanCreate')->with([
            'title' => 'Create Kekeluargaan'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'keluarga' => 'required|unique:m_kekeluargaan,keluarga'
        ]);

        DB::table('m_kekeluargaan')->insert([
            'keluarga' => $request->keluarga,
            'keterangan' => $request->keterangan,
            'created_at' => Carbon::now()
        ]);

        return redirect('master-kekeluargaan')->with([
            'title' => 'Master Kekeluargaan',
            'success' => 'Data Berhasil Ditambah'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('m_kekeluargaan')->where('id',$id)->get();
        return view('Master.Kekeluargaan.kekeluargaanEdit',['data' => $data])->with([
            'title' => 'Master Kekeluargaan'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('m_kekeluargaan')->where('id',$id)->update([
            'keluarga' => $request->keluarga,
            'keterangan' => $request->keterangan,
            'updated_at' => Carbon::now()
        ]);

        return redirect('master-kekeluargaan')->with([
            'title' => 'Master Kekeluargaan',
            'success' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('kekeluargaan')->where('id', $id)->delete();
        return redirect('master-kekeluargaan')->with([
            'title' => 'Master Kekeluargaan',
            'success' => 'Data Berhasil Dihapus'
        ]);
    }
}
