<?php

namespace App\Http\Controllers\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MasterStatusPerkawinanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('m_status_perkawinan')->get();
        return view('Master.StatusPerkawinan.statusPerkawinanShow', ['data' => $data])->with([
            'title' => 'Master Status Perkawinan'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Master.StatusPerkawinan.statusPerkawinanCreate')->with([
            'title' => 'Create Status Perkawinan'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'status_perkawinan' => 'required|unique:m_status_perkawinan,status_perkawinan'
        ]);

        DB::table('m_status_perkawinan')->insert([
            'status_perkawinan' => $request->status_perkawinan,
            'keterangan' => $request->keterangan,
            'created_at' => Carbon::now()
        ]);

        return redirect('master-status-perkawinan')->with([
            'title' => 'Master Status Perkawinan',
            'success' => 'Data Berhasil Ditambah'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('m_status_perkawinan')->where('id',$id)->get();
        return view('Master.StatusPerkawinan.statusPerkawinanEdit',['data' => $data])->with([
            'title' => 'Master Status Perkawinan'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('m_status_perkawinan')->where('id',$id)->update([
            'status_perkawinan' => $request->status_perkawinan,
            'keterangan' => $request->keterangan,
            'updated_at' => Carbon::now()
        ]);

        return redirect('master-status-perkawinan')->with([
            'title' => 'Master Status Perkawinan',
            'success' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('m_status_perkawinan')->where('id', $id)->delete();
        return redirect('master-status-perkawinan')->with([
            'title' => 'Master Status Perkawinan',
            'success' => 'Data Berhasil Dihapus'
        ]);
    }
}
