<?php

namespace App\Http\Controllers\Anggota\Kelompok;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class KelompokAnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tb_anggota_kelompok')->get();
        return view('Anggota.anggotaKelompok.anggotaKelompokShow',['data' => $data])->with([
            'title' => 'Anggota Kelompok'
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $kelompok = DB::table('tb_kelompok')->get();
        $jenisUsaha = DB::table('m_jenis_usaha')->get();
        $kekeluargaan = DB::table('m_kekeluargaan')->get();
        $jabatan = DB::table('m_jabatan')->get();

        return view('Anggota.anggotaKelompok.anggotaKelompokCreate',[
            'kelompok' => $kelompok,
            'jenisUsaha' => $jenisUsaha,
            'kekeluargaan' => $kekeluargaan,
            'jabatan' => $jabatan
            ])->with([
            'title' => 'Anggota Kelompok'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'nik' => 'unique:tb_anggota_kelompok,nik'
        // ]);
        $imageName = '';
        $input =$request->all();
        if ($image = $request->file('fileGambar')) {
            $destinationPath = 'images/logo-upk';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $imageName = $destinationPath.'/'.$profileImage;
            $image->move($destinationPath, $profileImage);
            $input['fileGambar'] = "$profileImage";
        }else{
            unset($input['fileGambar']);
        }

        // return $imageName;

        DB::table('tb_anggota_kelompok')->insert([
            'id_kelompok' => $request->kelompok,
            'nik' => $request->nik,
            'nama_lengkap' => $request->nama,
            'jenis_kelamin' => $request->jenisKelamin,
            'jabatan' => $request->jabatan,
            'tempat_lahir' => $request->tempatLahir,
            'tanggal_lahir' => $request->tanggalLahir,
            'telp' => $request->telp,
            'alamat' => $request->alamat,
            'status' => $request->status,
            'jumlah_tabungan' => $request->jumlahTabungan,
            'pengajuan_proposal' => $request->pengajuanProposal,
            'jenis_usaha' => $request->jenisUsaha,
            'jaminan' => $request->jaminan,
            'nama_penjamin' => $request->namaPenjamin,
            'jenis_kelamin_penjamin' => $request->jenisKelaminPenjamin,
            'status_keluarga' => $request->kekeluargaan,
            'photo' => $imageName,
            'created_at' => Carbon::Now()

        ]);
        return redirect('kelompok-anggota')->with('success', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('tb_anggota_kelompok')->where('id',$id)->get();
        $kelompok = DB::table('tb_kelompok')->get();
        $jenisUsaha = DB::table('m_jenis_usaha')->get();
        $kekeluargaan = DB::table('m_kekeluargaan')->get();
        $jabatan = DB::table('m_jabatan')->get();

        return view('Anggota.anggotaKelompok.anggotaKelompokEdit',[
            'data' => $data,
            'kelompok' => $kelompok,
            'jenisUsaha' => $jenisUsaha,
            'kekeluargaan' => $kekeluargaan,
            'jabatan' => $jabatan
        ])->with([
            'title' => 'Anggota Kelompok'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldImage = DB::select('SELECT photo from tb_anggota_kelompok where id='.$id.'');
        $imageName = $oldImage[0]->photo;



        $input =$request->all();
        if ($image = $request->file('fileGambar')) {
            $destinationPath = 'images/logo-upk';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $imageName = $destinationPath.'/'.$profileImage;
            if($oldImage[0]->photo){

                unlink($oldImage[0]->photo);
            }
            $image->move($destinationPath, $profileImage);
            $input['fileGambar'] = "$profileImage";
        }else{
            unset($input['fileGambar']);
        }

        DB::table('tb_anggota_kelompok')->where('id',$id)->update([
            'id_kelompok' => $request->kelompok,
            'nik' => $request->nik,
            'nama_lengkap' => $request->nama,
            'jenis_kelamin' => $request->jenisKelamin,
            'jabatan' => $request->jabatan,
            'tempat_lahir' => $request->tempatLahir,
            'tanggal_lahir' => $request->tanggalLahir,
            'telp' => $request->telp,
            'alamat' => $request->alamat,
            'status' => $request->status,
            'jumlah_tabungan' => $request->jumlahTabungan,
            'pengajuan_proposal' => $request->pengajuanProposal,
            'jenis_usaha' => $request->jenisUsaha,
            'jaminan' => $request->jaminan,
            'nama_penjamin' => $request->namaPenjamin,
            'jenis_kelamin_penjamin' => $request->jenisKelaminPenjamin,
            'status_keluarga' => $request->kekeluargaan,
            'photo' => $imageName,
            'updated_at' => Carbon::Now()

        ]);
        return redirect('kelompok-anggota')->with('success', 'Data Berhasil Dirubah');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldImage = DB::select('SELECT photo from tb_anggota_kelompok where id='.$id.'');
        $imageName = $oldImage[0]->photo;
        if($imageName){

            unlink($imageName);
        }

        DB::table('tb_anggota_kelompok')->where('id', $id)->delete();
        return redirect('kelompok-anggota')->with([
            'title' => 'Anggota Kelompok',
            'success' => 'Data Berhasil Dihapus'
        ]);    }
}
