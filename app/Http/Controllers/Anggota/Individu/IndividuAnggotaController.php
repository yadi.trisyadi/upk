<?php

namespace App\Http\Controllers\Anggota\Individu;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndividuAnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tb_anggota_individu')->get();
        return view('Anggota.anggotaIndividu.anggotaIndividuShow',['data' => $data])->with([
            'title' => 'Anggota Individu'
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $desa = DB::table('tb_desa')->get();
        $jenisUsaha = DB::table('m_jenis_usaha')->get();
        $jenisKegiatan = DB::table('m_jenis_kegiatan')->get();
        return view('Anggota.anggotaIndividu.anggotaIndividuCreate',[
            'desa' => $desa,
            'jenisUsaha' => $jenisUsaha,
            'jenisKegiatan' => $jenisKegiatan,
            ])->with([
            'title' => 'Anggota Individu'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imageName = '';
        $input =$request->all();
        if ($image = $request->file('fileGambar')) {
            $destinationPath = 'images/logo-upk';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $imageName = $destinationPath.'/'.$profileImage;
            $image->move($destinationPath, $profileImage);
            $input['fileGambar'] = "$profileImage";
        }else{
            unset($input['fileGambar']);
        }

        // return $imageName;

        DB::table('tb_anggota_individu')->insert([
            'id' => $request->id,
            'tanggal_masuk' => $request->tanggalMasuk,
            'kode_registrasi' => $request->kodeRegistrasi,
            'no_kk' => $request->noKK,
            'no_nik' => $request->nik,
            'nama_lengkap' => $request->namaLengkap,
            'jenis_kelamin' => $request->jenisKelamin,
            'tempat_lahir' => $request->tempatLahir,
            'tanggal_lahir' => $request->tanggalLahir,
            'telp' => $request->telp,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'desa' => $request->desa,
            'kecamatan' => $request->kecamatan,
            'kabupaten' => $request->kabupaten,
            'propinsi' => $request->propinsi,
            'kode_pos' => $request->kodePos,
            'keanggotaan' => $request->keanggotaan,
            'status' => $request->status,
            'tanggal_keluar' => $request->tanggalKeluar,
            'agama' => $request->agama,
            'status_perkawinan' => $request->statusPerkawinan,
            'pekerjaan' => $request->pekerjaan,
            'jumlah_tabungan' => $request->jumlahTabungan,
            'jumlah_pinjaman' => $request->jumlahPinjaman,
            'jenis_usaha' => $request->jenisUsaha,
            'catatan_khusus' => $request->catatanKhusus,
            'tipe_anggota' => $request->tipeAnggota,
            'photo' => $imageName,
            'created_at' => Carbon::Now()

        ]);
        return redirect('individu-anggota')->with('success', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('tb_anggota_individu')->where('id',$id)->get();
        return view('Anggota.anggotaIndividu.anggotaIndividuDetail',['data' => $data])->with([
            'title' => 'Anggota Individu'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $desa = DB::table('tb_desa')->get();
        $jenisUsaha = DB::table('m_jenis_usaha')->get();
        $jenisKegiatan = DB::table('m_jenis_kegiatan')->get();
        $data = DB::table('tb_anggota_individu')->where('id',$id)->get();
        return view('Anggota.anggotaIndividu.anggotaIndividuEdit',[
            'data' => $data,
            'desa' => $desa,
            'jenisUsaha' => $jenisUsaha,
            'jenisKegiatan' => $jenisKegiatan,
            ])->with([
            'title' => 'Anggota Individu'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $imageName = '';
        $input =$request->all();
        if ($image = $request->file('fileGambar')) {
            $destinationPath = 'images/logo-upk';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $imageName = $destinationPath.'/'.$profileImage;
            $image->move($destinationPath, $profileImage);
            $input['fileGambar'] = "$profileImage";
        }else{
            unset($input['fileGambar']);
        }

        DB::table('tb_anggota_individu')->where('id', $id)->update([
            'tanggal_masuk' => $request->tanggalMasuk,
            'kode_registrasi' => $request->kodeRegistrasi,
            'no_kk' => $request->noKK,
            'no_nik' => $request->nik,
            'nama_lengkap' => $request->namaLengkap,
            'jenis_kelamin' => $request->jenisKelamin,
            'tempat_lahir' => $request->tempatLahir,
            'tanggal_lahir' => $request->tanggalLahir,
            'telp' => $request->telp,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'desa' => $request->desa,
            'kecamatan' => $request->kecamatan,
            'kabupaten' => $request->kabupaten,
            'propinsi' => $request->propinsi,
            'kode_pos' => $request->kodePos,
            'keanggotaan' => $request->keanggotaan,
            'status' => $request->status,
            'tanggal_keluar' => $request->tanggalKeluar,
            'agama' => $request->agama,
            'status_perkawinan' => $request->statusPerkawinan,
            'pekerjaan' => $request->pekerjaan,
            'jumlah_tabungan' => $request->jumlahTabungan,
            'jumlah_pinjaman' => $request->jumlahPinjaman,
            'jenis_usaha' => $request->jenisUsaha,
            'catatan_khusus' => $request->catatanKhusus,
            'tipe_anggota' => $request->tipeAnggota,
            'photo' => $imageName,
            'updated_at' => Carbon::now()
        
        ]);


        return redirect('individu-anggota')->with([

            'title' => 'Anggota Individu',
            'success' => 'Data Berhasil Di Update'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        DB::table('tb_anggota_individu')->where('id', $id)->delete();
        return redirect('individu-anggota')->with([
            'title' => 'Anggota Kelompok',
            'success' => 'Data Berhasil Dihapus'
        ]);

    }
}
