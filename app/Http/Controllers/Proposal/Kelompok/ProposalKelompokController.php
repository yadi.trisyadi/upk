<?php

namespace App\Http\Controllers\Proposal\Kelompok;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;


class ProposalKelompokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registrasi = DB::select("SELECT a.id,a.tanggal_proposal,a.kode_registrasi,b.nama_kelompok,b.desa,a.jumlah_pengajuan,a.jumlah_rekomendasi,a.jumlah_pemanfaat from tb_proposal_kelompok a, tb_kelompok b where a.id_kelompok = b.id and a.status='2'");

        $verifikasi = DB::select("SELECT a.id,a.tanggal_proposal,a.kode_registrasi,b.nama_kelompok,b.desa,a.jumlah_pengajuan,a.jumlah_rekomendasi,a.jumlah_pemanfaat from tb_proposal_kelompok a, tb_kelompok b where a.id_kelompok = b.id and a.status='3'");

        $penetapan = DB::select("SELECT a.id,a.tanggal_proposal,a.kode_registrasi,b.nama_kelompok,b.desa,a.jumlah_pengajuan,a.jumlah_rekomendasi,a.jumlah_pemanfaat from tb_proposal_kelompok a, tb_kelompok b where a.id_kelompok = b.id and a.status='4'");

        $countRegistrasi = DB::table('tb_proposal_kelompok')->where('status',2)->count();
        $countVerifikasi = DB::table('tb_proposal_kelompok')->where('status',3)->count();
        $countPenetapan = DB::table('tb_proposal_kelompok')->where('status',4)->count();


        return view('Proposal.Kelompok.proposalShow',[
            'registrasi' => $registrasi,
            'verifikasi' => $verifikasi,
            'penetapan' => $penetapan,
            'countRegistrasi' => $countRegistrasi,
            'countVerifikasi' => $countVerifikasi,
            'countPenetapan' => $countPenetapan
            ])->with([
            'title' => 'Proposal'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::select('select id,kode_kelompok,jenis_kelompok,nama_kelompok,desa,nama_ketua,telp_kelompok,status from tb_kelompok');
        return view('Proposal.Kelompok.listKelompok',['data' => $data])->with([
            'title' => 'Daftar Kelompok'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show($id)
    {
        $data = DB::table('tb_kelompok')->where('id', $id)->get();
        $jumlahAnggota = DB::table('tb_anggota_kelompok')->where('id_kelompok',$id)->count();
        $alokasiPinjaman = DB::table('tb_anggota_kelompok')->where('id_kelompok',$id)->sum('pengajuan_proposal');
        $dataAnggota = DB::table('tb_anggota_kelompok')->where('id_kelompok',$id)->get();
        return view('Proposal.Kelompok.listAnggotaKelompok',[
            'data' => $data,
            'jumlahAnggota' => $jumlahAnggota,
            'alokasiPinjaman' => $alokasiPinjaman,
            'dataAnggota' => $dataAnggota
            ])->with([
            'title' => 'Anggota Kelompok'
        ]);
    }

    public function tambahAnggotaKelompok($id){
        $kelompok = DB::table('tb_kelompok')->where('id',$id)->get();
        $jenisUsaha = DB::table('m_jenis_usaha')->get();
        $kekeluargaan = DB::table('m_kekeluargaan')->get();
        $jabatan = DB::table('m_jabatan')->get();
        return view('Proposal.Kelompok.tambahAnggotaKelompok')->with([
            'title' => 'Tambah Anggota',
            'id' => $id,
            'kelompok' => $kelompok,
            'jenisUsaha' => $jenisUsaha,
            'kekeluargaan' => $kekeluargaan,
            'jabatan' => $jabatan
        ]);
    }

    public function storeAnggotaKelompok(Request $request){
        $imageName = '';
        $input =$request->all();
        if ($image = $request->file('fileGambar')) {
            $destinationPath = 'images/logo-upk';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $imageName = $destinationPath.'/'.$profileImage;
            $image->move($destinationPath, $profileImage);
            $input['fileGambar'] = "$profileImage";
        }else{
            unset($input['fileGambar']);
        }

        // return $imageName;

        DB::table('tb_anggota_kelompok')->insert([
            'id_kelompok' => $request->kelompok,
            'nik' => $request->nik,
            'nama_lengkap' => $request->nama,
            'jenis_kelamin' => $request->jenisKelamin,
            'jabatan' => $request->jabatan,
            'tempat_lahir' => $request->tempatLahir,
            'tanggal_lahir' => $request->tanggalLahir,
            'telp' => $request->telp,
            'alamat' => $request->alamat,
            'status' => $request->status,
            'jumlah_tabungan' => $request->jumlahTabungan,
            'pengajuan_proposal' => $request->pengajuanProposal,
            'jenis_usaha' => $request->jenisUsaha,
            'jaminan' => $request->jaminan,
            'nama_penjamin' => $request->namaPenjamin,
            'jenis_kelamin_penjamin' => $request->jenisKelaminPenjamin,
            'status_keluarga' => $request->kekeluargaan,
            'photo' => $imageName,
            'created_at' => Carbon::Now()

        ]);
        return redirect('proposal-kelompok/'.$request->kelompok.'')->with('success', 'Data Berhasil Ditambah');
    }

    public function addProposal(Request $request){
        $result = array();
        $sum = 0;
        foreach($request->proposal as $key => $value){
            $idAnggota = $request->idAnggota[$key];
            $jumlahProposal = $value;
            // echo "$idAnggota - $jumlahProposal<br>";
            if($jumlahProposal > 0){
                $result[] = array('idAnggota' => $idAnggota, 'jumlahProposal'=>$jumlahProposal);
            }

            $sum += $jumlahProposal;
        }

        $count = count($result);
        return view('Proposal.Kelompok.proposalCreate',[
            'idKelompok' => $request->idKelompok,
            'data' => $result,
            'jumlahPemanfaat' => $count,
            'jumlahPengajuan' => $sum
        ])->with([
            'title' => 'Proposal'
        ]);
    }


    public function bunga($jumlahPinjaman, $sukuBunga, $jangkaWaktu){
        $sukuBunga = $sukuBunga/100;
        $result = ($jumlahPinjaman * $sukuBunga) / 12;
        return $result;
    }

    public function pokok($jumlahPinjaman, $jangkaWaktu){
        $result = $jumlahPinjaman/$jangkaWaktu;
        return $result;
    }

    public function angsuran($bunga,$pokok){
        $result = $bunga + $pokok;
        return $result;
    }

    public function storeProposal(Request $request){

        $jumlahPinjaman = $request->jumlahPengajuan;
        $jangkaWaktu = $request->jangkaWaktu;
        $sukuBunga = $request->jasa;

        $bunga = round($this->bunga($jumlahPinjaman,$sukuBunga,$jangkaWaktu));
        $pokok = round($this->pokok($jumlahPinjaman,$jangkaWaktu));
        $angsuran = $this->angsuran($bunga,$pokok);
        $idProposal = DB::table('tb_proposal_kelompok')->insertGetID([
            'id_kelompok' => $request->idKelompok,
            'tanggal_proposal' => $request->tanggal,
            'kode_registrasi' => $request->kodeRegistrasi,
            'nomor_proposal' => $request->nomorProposal,
            'jumlah_pengajuan' => $request->jumlahPengajuan,
            'jumlah_pemanfaat' => $request->jumlahPemanfaat,
            'nomor_rekom_kades' => $request->noRekomKades,
            'pokok' => $pokok,
            'bunga' => $bunga,
            'angsuran' => $angsuran,
            'jasa' => $request->jasa,
            'jangka_waktu' => $request->jangkaWaktu,
            'status' => '2',
            'sistem_angsuran' => $request->sistemAngsuran,
            'created_at' => Carbon::now()
        ]);

        foreach ($request->idAnggota as $key => $value) {
            $idAnggota = $value;
            $jumlahPengajuan = $request->jumlahProposal[$key];

            DB::table('tb_proposal_detail')->insert([
                'id_proposal' => $idProposal,
                'kode_registrasi_proposal' => $request->kodeRegistrasi,
                'nomor_proposal' => $request->nomorProposal,
                'id_anggota' => $idAnggota,
                'jumlah_pengajuan' => $jumlahPengajuan,
                'status' => '2',
                'created_at' => Carbon::now()

            ]);
        }

        return redirect('proposal-finished/'.$idProposal);


    }

    public function proposalFinished($id){

        $idKelompok = DB::select("SELECT id_kelompok FROM tb_proposal_kelompok where id='$id'");

        // return $idKelompok;
        $data = DB::table('tb_kelompok')->where('id',$idKelompok[0]->id_kelompok)->get();
        return view('Proposal.Kelompok.proposalFinished',['data' => $data])->with([
            'title' => 'Proposal'
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
