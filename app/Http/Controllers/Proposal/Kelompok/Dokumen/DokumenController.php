<?php

namespace App\Http\Controllers\Proposal\Kelompok\Dokumen;

// use Barryvdh\DomPDF\PDF;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class DokumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifikasi($id)
    {
        $dataKelompok = DB::SELECT("SELECT a.id,a.tanggal_proposal,a.jumlah_pengajuan,a.jasa,a.jangka_waktu,a.kode_registrasi, b.nama_kelompok, b.desa, b.nama_ketua,b.alamat_kelompok,b.telp_kelompok,b.jenis_kelompok,b.jenis_usaha,b.jenis_kegiatan,b.tingkat,b.fungsi FROM tb_proposal_kelompok a, tb_kelompok b where a.id='$id' and a.id_kelompok=b.id");

        // $dataAnggota = DB::table('tb_proposal_detail')->where('id_proposal',$id)->get();
        $dataAnggota = DB::select("SELECT a.nama_lengkap,b.jumlah_pengajuan,b.jumlah_rekomendasi from tb_anggota_kelompok a, tb_proposal_detail b where a.id=b.id_anggota and b.id_proposal='$id' and b.status='0'");
        $jumlahPengajuan = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 0,
        ])->sum('jumlah_pengajuan');

        $data = array('dataKelompok' => $dataKelompok, 'dataAnggota' => $dataAnggota,'jumlahTotal' => $jumlahPengajuan);

        // return view('Proposal.Kelompok.Dokumen.suratVerifikasi',$id);
        $pdf = PDF::loadview('Proposal.Kelompok.Dokumen.suratVerifikasi',$data);

        return $pdf->stream('verifikasi-proposal.pdf');
    }

    public function anggotaKelompok($id)
    {
        $dataKelompok = DB::SELECT("SELECT a.id,a.id_kelompok, a.tanggal_proposal,a.jumlah_pengajuan,a.jasa,a.jangka_waktu,a.kode_registrasi, b.nama_kelompok, b.desa, b.nama_ketua,b.alamat_kelompok,b.telp_kelompok,b.jenis_kelompok,b.jenis_usaha,b.jenis_kegiatan,b.tingkat,b.fungsi FROM tb_proposal_kelompok a, tb_kelompok b where a.id='$id' and a.id_kelompok=b.id");

        // $dataAnggota = DB::table('tb_proposal_detail')->where('id_proposal',$id)->get();
        $dataAnggota = DB::table('tb_anggota_kelompok')->where('id_kelompok',$dataKelompok[0]->id_kelompok)->get();
        $jumlahPengajuan = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 0,
        ])->sum('jumlah_pengajuan');

        $data = array('dataKelompok' => $dataKelompok, 'dataAnggota' => $dataAnggota,'jumlahTotal' => $jumlahPengajuan);

        // return view('Proposal.Kelompok.Dokumen.suratVerifikasi',$id);
        $pdf = PDF::loadview('Proposal.Kelompok.Dokumen.daftarAnggotaKelompok',$data);

        return $pdf->stream('daftar-anggota-kelompok.pdf');
    }
    public function suratRekomendasi($id)
    {
        $dataKelompok = DB::SELECT("SELECT a.id,a.id_kelompok, a.tanggal_proposal,a.jumlah_pengajuan,a.jasa,a.jangka_waktu,a.kode_registrasi, b.nama_kelompok, b.desa, b.nama_ketua,b.alamat_kelompok,b.telp_kelompok,b.jenis_kelompok,b.jenis_usaha,b.jenis_kegiatan,b.tingkat,b.fungsi FROM tb_proposal_kelompok a, tb_kelompok b where a.id='$id' and a.id_kelompok=b.id");
        $profile = DB::table('tb_profil')->get();

        // $dataAnggota = DB::table('tb_proposal_detail')->where('id_proposal',$id)->get();
        $dataAnggota = DB::table('tb_anggota_kelompok')->where('id_kelompok',$dataKelompok[0]->id_kelompok)->get();
        $jumlahPengajuan = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 0,
        ])->sum('jumlah_pengajuan');

        $data = array('profil' => $profile, 'dataAnggota' => $dataAnggota);

        // return view('Proposal.Kelompok.Dokumen.suratVerifikasi',$id);
        $pdf = PDF::loadview('Proposal.Kelompok.Dokumen.suratRekomendasi',$data);

        return $pdf->stream('daftar-anggota-kelompok.pdf');
    }
    public function permohonanKredit($id)
    {
        $dataKelompok = DB::SELECT("SELECT a.id,a.id_kelompok, a.tanggal_proposal,a.jumlah_pengajuan,a.jasa,a.jangka_waktu,a.kode_registrasi, b.nama_kelompok, b.desa, b.nama_ketua,b.alamat_kelompok,b.telp_kelompok,b.jenis_kelompok,b.jenis_usaha,b.jenis_kegiatan,b.tingkat,b.fungsi FROM tb_proposal_kelompok a, tb_kelompok b where a.id='$id' and a.id_kelompok=b.id");
        $profile = DB::table('tb_profil')->get();

        // $dataAnggota = DB::table('tb_proposal_detail')->where('id_proposal',$id)->get();
        $dataAnggota = DB::table('tb_anggota_kelompok')->where('id_kelompok',$dataKelompok[0]->id_kelompok)->get();
        $jumlahPengajuan = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 0,
        ])->sum('jumlah_pengajuan');

        $data = array('profil' => $profile, 'dataAnggota' => $dataAnggota);

        // return view('Proposal.Kelompok.Dokumen.suratVerifikasi',$id);
        $pdf = PDF::loadview('Proposal.Kelompok.Dokumen.suratPermohonanKredit',$data);

        return $pdf->stream('daftar-anggota-kelompok.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
