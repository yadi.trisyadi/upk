<?php

namespace App\Http\Controllers\Proposal\Kelompok;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class VerifikasiProposalController extends Controller
{
    public function verifikasiProposal($id){
        $data = DB::table('tb_proposal_kelompok')->where('id',$id)->get();
        $dataKelompok = DB::table('tb_kelompok')->where('id',$data[0]->id_kelompok)->get();
        $jumlahAnggota = DB::table('tb_anggota_kelompok')->where('id_kelompok',$data[0]->id_kelompok)->count();
        $jumlahPemanfaat = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 2
            ])->count();



        $alokasiPinjaman = DB::table('tb_proposal_detail')->where([
            'id_proposal' => $id,
            'status' => 2
            ])->sum('jumlah_pengajuan');
        // $dataAnggota = DB::select("SELECT a.id,a.nik,a.nama_lengkap,a.jabatan,a.jenis_usaha,b.jumlah_pengajuan FROM tb_anggota_kelompok a, tb_proposal_detail b where a.id = b.id_anggota and b.status='0' and b.id_proposal='$id'");


        $dataAnggota = DB::select("SELECT a.id,a.nik,a.nama_lengkap,a.jabatan,a.jenis_usaha,b.jumlah_pengajuan FROM tb_anggota_kelompok a, tb_proposal_detail b where a.id = b.id_anggota and b.status='2' and b.id_proposal='$id'");
        return view('Proposal.Verifikasi.verifikasiShow',[
            'idKelompok' => $data[0]->id_kelompok,
            'idProposal' => $data[0]->id,
            'dataKelompok' => $dataKelompok,
            'jumlahAnggota' => $jumlahAnggota,
            'jumlahPemanfaat' => $jumlahPemanfaat,
            'alokasiPinjaman' => $alokasiPinjaman,
            'dataAnggota' => $dataAnggota
        ])->with([
            'title' => 'Verifikasi Proposal'
        ]);
    }

    public function verifikasiProposalAdd(Request $request){
       $idKelompok = $request->idKelompok;
       $idProposal = $request->idProposal;
    //    $dataPropposal = DB::table('tb_proposal_kelompok')->where('id',$idProposal);
       $dataPropposal = DB::select("SELECT * from tb_proposal_kelompok WHERE id='$idProposal'");
       $alokasiPinjaman = $request->grandTotalProposal;

       $sum = 0;
        foreach($request->rekomendasi as $key => $value){
            $idAnggota = $request->idAnggota[$key];
            $jumlahProposal = $value;
            // echo "$idAnggota - $jumlahProposal<br>";
            if($jumlahProposal > 0){
                $result[] = array('idAnggota' => $idAnggota, 'jumlahProposal'=>$jumlahProposal);
            }

            $sum += $jumlahProposal;
        }

        $count = count($result);
// return $result;
        return view('Proposal.Verifikasi.verifikasiCreate',[
            'dataProposal' => $dataPropposal,
            'idKelompok' => $idKelompok,
            'alokasiPinjaman' => $alokasiPinjaman,
            'anggota' => $result,
            'jumlahPemanfaat' => $count
        ])->with([
            'title' => 'Verifikasi Proposal'
        ]);
    }

    public function verifikasiProposalStore(Request $request){


        //update ke proposal kelompok

        // return $request->keterangan;
        $jumlahPinjaman = $request->pinjaman;
        $jangkaWaktu = $request->jangkaWaktuVerifikasi;
        $sukuBunga = $request->jasaVerifikasi;

        $bunga = round($this->bunga($jumlahPinjaman,$sukuBunga,$jangkaWaktu));
        $pokok = round($this->pokok($jumlahPinjaman,$jangkaWaktu));
        $angsuran = $this->angsuran($bunga,$pokok);


        $updateProposalKel = DB::table('tb_proposal_kelompok')->where([
            'id' => $request->idProposal,
            'status' => 2

            ])->update([
                'jumlah_rekomendasi' => $request->pengajuan,
                'status' => 3
            ]);

        if($updateProposalKel > 0){
            foreach ($request->idAnggota as $key => $value) {
                $idAnggota = $value;
                $jumlahPengajuan = $request->jumlahProposal[$key];

                DB::table('tb_proposal_detail')->where([
                    'id_proposal' => $request->idProposal,
                    'id_anggota' => $idAnggota,
                    'status' => 2
                ]) ->update([
                    'jumlah_rekomendasi' => $jumlahPengajuan,
                    'status' => 3,
                    'updated_at' => Carbon::now()

                ]);


            }

            $insertProposalKelVerfikasi = DB::table('tb_proposal_verifikasi_kelompok')->insertGetID([
                'id_kelompok' => $request->idKelompok,
                'tanggal_verifikasi' => $request->tanggalVerifikasi,
                'kode_registrasi' => $request->kodeRegistrasiVerifikasi,
                'jumlah_pengajuan' => $request->pinjaman,
                'jumlah_pemanfaat' => $request->pemanfaatVerifikasi,
                'pokok' => $pokok,
                'bunga' => $bunga,
                'angsuran' => $angsuran,
                'jasa' => $request->jasaVerifikasi,
                'jangka_waktu' => $request->jangkaWaktuVerifikasi,
                'sistem_angsuran' => $request->sistemAngsuranVerifikasi,
                'status' => 3,
                'keterangan' => $request->keterangan,
                'memo' => $request->memo,
                'created_at' => Carbon::now()
            ]);

            foreach ($request->idAnggota as $keyVer => $valueVer) {
                $idAnggota = $valueVer;
                $jumlahPengajuan = $request->jumlahProposal[$keyVer];
                DB::table('tb_proposal_verifikasi_detail')->insert([
                    'id_proposal' => $request->idProposal,
                    'id_verifikasi' => $insertProposalKelVerfikasi,
                    'kode_registrasi_proposal' => $request->kodeRegistrasiVerifikasi,
                    'nomor_proposal' => $request->nomorProposal,
                    'id_anggota' => $idAnggota,
                    'jumlah_pengajuan' => $jumlahPengajuan,
                    'status' => 3,
                    'created_at' => Carbon::now()

                ]);
            }
        }

        return redirect('verifikasi-finished/'.$insertProposalKelVerfikasi);
    }

    public function verifikasiFinished($id){


      $dataVerifikasi = DB::select("SELECT * from tb_proposal_verifikasi_kelompok where id='$id'");


      $idKelompok  = $dataVerifikasi[0]->id_kelompok;
      $idProposal = $dataVerifikasi[0]->id;
    //   return $dataVerifikasi;


      $dataProposal = DB::table('tb_proposal_kelompok')->where('id',$idProposal)->get();
      $dataAnggota = DB::select("SELECT a.nik,a.nama_lengkap,a.jenis_kelamin,b.jumlah_pengajuan,b.jumlah_rekomendasi from tb_anggota_kelompok a, tb_proposal_detail b where a.id=b.id_anggota and b.id_proposal = '$idProposal'");

// return $idKelompok;
// return $result;
        return view('Proposal.Verifikasi.verifikasiResult',[
            'dataVerifikasi' => $dataVerifikasi,
            'dataProposal' => $dataProposal,
            'dataAnggota' => $dataAnggota,

        ])->with([
            'title' => 'Verifikasi Proposal'
        ]);
    }

    public function bunga($jumlahPinjaman, $sukuBunga, $jangkaWaktu){
        $sukuBunga = $sukuBunga/100;
        $result = ($jumlahPinjaman * $sukuBunga) / 12;
        return $result;
    }

    public function pokok($jumlahPinjaman, $jangkaWaktu){
        $result = $jumlahPinjaman/$jangkaWaktu;
        return $result;
    }

    public function angsuran($bunga,$pokok){
        $result = $bunga + $pokok;
        return $result;
    }

}
