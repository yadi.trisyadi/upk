<?php

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


function getDesa($desa_upk){
    $getKecamatanCode = DB::select("SELECT `district_code` from indonesia_villages where id='$desa_upk'");
    $desa = DB::table('indonesia_villages')->where([
        'district_code' => $getKecamatanCode[0]->district_code
    ])->get();

    return $desa;
};

function getNameDesa($id){
    $result = DB::table('indonesia_villages')->where([
        'code' => $id
    ])->get();

    return $result;
}

function namaDesaKelompok($id){
    $result = DB::table('indonesia_villages')->where([
        'code' => $id
    ])->get();

    return $result[0]->name;
}

function getStatus($id){
    if($id == 0){
        $status = 'Aktif';
    }elseif($id == 1){
        $status = 'Pasif';
    }elseif($id == 2){
        $status = 'Registrasi';
    }elseif($id == 3){
        $status = 'Verifikasi';
    }else{
        $status = 'Menunggu';
    }
    return $status;
}

function getAwalPinjaman($id){
    if($id == 1){
        $result = 'Reguler';
    }elseif($id == 2){
        $result = 'Perguliran';
    }else{
        $result = 'Lain-lain';
    }
    return $result;
}

function getFungsi($id){
    if($id == 1){
        $result = 'Penyalur';
    }else{
        $result = 'Pengelola';
    }

    return $result;
}

function getTingkat($id){
    if($id == 1){
        $result = 'Pemula';
    }elseif($id == 2){
        $result = 'Berkembang';
    }else{
        $result = 'Mandiri';
    }
    return $result;
}

function getNameJenisUsaha($id){
    $result = DB::table('m_jenis_usaha')->where('id',$id)->get();
    return $result[0]->jenis_usaha;
}
function getNameJenisKegiatan($id){
    $result = DB::table('m_jenis_kegiatan')->where('id',$id)->get();
    return $result[0]->jenis_kegiatan;
}

function getNamaKelompok($id){
    $result = DB::table('tb_kelompok')->where('id',$id)->get();
    return $result[0]->nama_kelompok;
}

function getNameAgama($id){
    $result = DB::table('m_agama')->where('id',$id)->get();
    return $result[0]->agama;
}

function getTipeAnggota($id){
    if($id == 1){
        $result = 'SPP';
    }elseif($id == 2){
        $result = 'UEP';
    }else{
        $result = 'Perorangan';
    }
    return $result;
}

function getJenisKelamin($id){
    if($id == 1){
        $result = 'Laki-Laki';
    }else{
        $result = 'Perempuan';
    }

    return $result;
}

function getKeanggotaan($id){
    if($id == 1){
        $result = 'Umum';
    }elseif($id == 2){
        $result = 'Pengurus';
    }else{
        $result = 'Karyawan';
    }
    return $result;
}

function getJenisKelompok($id){
    if($id == 1){
        $result = 'SPP';
    }else{
        $result = 'UEP';
    }
}

function getJabatan($id){
    $data = DB::table('m_jabatan')->where('id',$id)->get();
    $result = $data[0]->jabatan;
    return $result;

}

function getTanggalIndo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}