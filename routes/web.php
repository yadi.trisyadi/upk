<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/config-clear', function() {
    Artisan::call('config:clear');
    return 'Configuration cache cleared!';
});

Route::get('/config-cache', function() {
    Artisan::call('config:cache');
    return 'Configuration cache cleared! <br> Configuration cached successfully!';
});

Route::get('/cache-clear', function() {
    Artisan::call('cache:clear');
    return 'Application cache cleared!';
});

Route::get('/view-cache', function() {
    Artisan::call('view:cache');
    return 'Compiled views cleared! <br> Blade templates cached successfully!';
});

Route::get('/view-clear', function() {
    Artisan::call('view:clear');
    return 'Compiled views cleared!';
});

Route::get('/route-cache', function() {
    Artisan::call('route:cache');
    return 'Route cache cleared! <br> Routes cached successfully!';
});

Route::get('/route-clear', function() {
    Artisan::call('route:clear');
    return 'Route cache cleared!';
});

Route::get('/storage-link', function() {
    Artisan::call('storage:link');
    return 'The links have been created.';
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'root'])->name('root');
// Route::get('/testing', [App\Http\Controllers\Pengaturan\PengaturanProfileController::class, 'index'])->name('index');

Route::resource('master-jenis-kegiatan', App\Http\Controllers\Master\MasterJenisKegiatanController::class);
Route::resource('master-jenis-usaha', App\Http\Controllers\Master\MasterJenisUsahaController::class);
Route::resource('master-pekerjaan', App\Http\Controllers\Master\MasterPekerjaanController::class);
Route::resource('master-agama', App\Http\Controllers\Master\MasterAgamaController::class);
Route::resource('master-kekeluargaan', App\Http\Controllers\Master\MasterKekeluargaanController::class);
Route::resource('master-status-perkawinan', App\Http\Controllers\Master\MasterStatusPerkawinanController::class);

//Update User Details
Route::post('/update-profile/{id}', [App\Http\Controllers\HomeController::class, 'updateProfile'])->name('updateProfile');
Route::post('/update-password/{id}', [App\Http\Controllers\HomeController::class, 'updatePassword'])->name('updatePassword');

// Route::get('{any}', [App\Http\Controllers\HomeController::class, 'index1'])->name('index1');

//Language Translation
Route::get('index/{locale}', [App\Http\Controllers\HomeController::class, 'lang']);
// Route::get('/pengaturan-profile', [\App\Http\Controllers\Pengaturan\PengaturanProfileController::class, 'index'])->name('index');
Route::resource('pengaturan-profile', App\Http\Controllers\Pengaturan\PengaturanProfileController::class);
Route::resource('pengaturan-kecamatan', App\Http\Controllers\Pengaturan\PengaturanKecamatanController::class);
Route::resource('pengaturan-desa', App\Http\Controllers\Pengaturan\PengaturanDesaController::class);
Route::resource('pengaturan-pengurus', App\Http\Controllers\Pengaturan\PengaturanPengurusController::class);


Route::resource('kelompok-list', App\Http\Controllers\Anggota\Kelompok\KelompokListController::class);
Route::resource('kelompok-anggota', App\Http\Controllers\Anggota\Kelompok\KelompokAnggotaController::class);
Route::resource('individu-anggota', App\Http\Controllers\Anggota\Individu\IndividuAnggotaController::class);

Route::resource('proposal-kelompok', App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class);

Route::get('/tambah-anggota-proposal/{id}', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'tambahAnggotaKelompok'])->name('tambahAnggotaKelompok');

Route::post('/store-tambah-anggota-proposal', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'storeAnggotaKelompok'])->name('storeAnggotaKelompok');
Route::post('/add-proposal', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'addProposal'])->name('addProposal');
Route::post('/store-proposal', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'storeProposal'])->name('storeProposal');
Route::get('/proposal-finished/{id}', [App\Http\Controllers\Proposal\Kelompok\ProposalKelompokController::class, 'proposalFinished'])->name('proposalFinished');

Route::get('/verifikasi-proposal/{id}', [App\Http\Controllers\Proposal\Kelompok\VerifikasiProposalController::class, 'verifikasiProposal'])->name('verifikasiProposal');
Route::post('verifikasi-proposal-store', [App\Http\Controllers\Proposal\Kelompok\VerifikasiProposalController::class, 'verifikasiProposalStore'])->name('verifikasiProposalStore');
Route::post('verifikasi-proposal-add', [App\Http\Controllers\Proposal\Kelompok\VerifikasiProposalController::class, 'verifikasiProposalAdd'])->name('verifikasiProposalAdd');
Route::post('verifikasi-proposal-create', [App\Http\Controllers\Proposal\Kelompok\VerifikasiProposalController::class, 'verifikasiProposalStore'])->name('verifikasiProposalStore');

Route::get('verifikasi-finished/{id}', [App\Http\Controllers\Proposal\Kelompok\VerifikasiProposalController::class, 'verifikasiFinished'])->name('verifikasiFinished');


// Route::resource('/verifikasi-proposal', App\Http\Controllers\Proposal\Kelompok\VerifikasiProposalController::class);



Route::get('/dokumen-verifikasi/{id}', [App\Http\Controllers\Proposal\Kelompok\Dokumen\DokumenController::class, 'verifikasi'])->name('verifikasi');
Route::get('/dokumen-anggota-kelompok/{id}', [App\Http\Controllers\Proposal\Kelompok\Dokumen\DokumenController::class, 'anggotaKelompok'])->name('anggotaKelompok');
Route::get('/dokumen-surat-rekomendasi/{id}', [App\Http\Controllers\Proposal\Kelompok\Dokumen\DokumenController::class, 'suratRekomendasi'])->name('suratRekomendasi');
Route::get('/dokumen-permohonan-kredit/{id}', [App\Http\Controllers\Proposal\Kelompok\Dokumen\DokumenController::class, 'permohonanKredit'])->name('permohonanKrefit');






// Route::get('provinces', 'DependentDropdownController@provinces')->name('provinces');
Route::get('/provinces', [\App\Http\Controllers\DependantDropdownController::class, 'provinces'])->name('provinces');
Route::get('/cities', [\App\Http\Controllers\DependantDropdownController::class, 'cities'])->name('cities');
Route::get('/districts', [\App\Http\Controllers\DependantDropdownController::class, 'districts'])->name('districts');
Route::get('/villages', [\App\Http\Controllers\DependantDropdownController::class, 'villages'])->name('villages');


